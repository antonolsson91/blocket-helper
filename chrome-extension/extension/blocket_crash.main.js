!function(a){"function"==typeof define&&define.amd?define(["jquery"],a):"object"==typeof module&&"object"==typeof module.exports?module.exports=a(require("jquery")):a(jQuery)}(function(a){return function(b){"use strict";var c=b.tablesorter={version:"2.28.3",parsers:[],widgets:[],defaults:{theme:"default",widthFixed:!1,showProcessing:!1,headerTemplate:"{content}",onRenderTemplate:null,onRenderHeader:null,cancelSelection:!0,tabIndex:!0,dateFormat:"mmddyyyy",sortMultiSortKey:"shiftKey",sortResetKey:"ctrlKey",usNumberFormat:!0,delayInit:!1,serverSideSorting:!1,resort:!0,headers:{},ignoreCase:!0,sortForce:null,sortList:[],sortAppend:null,sortStable:!1,sortInitialOrder:"asc",sortLocaleCompare:!1,sortReset:!1,sortRestart:!1,emptyTo:"bottom",stringTo:"max",duplicateSpan:!0,textExtraction:"basic",textAttribute:"data-text",textSorter:null,numberSorter:null,initWidgets:!0,widgetClass:"widget-{name}",widgets:[],widgetOptions:{zebra:["even","odd"]},initialized:null,tableClass:"",cssAsc:"",cssDesc:"",cssNone:"",cssHeader:"",cssHeaderRow:"",cssProcessing:"",cssChildRow:"tablesorter-childRow",cssInfoBlock:"tablesorter-infoOnly",cssNoSort:"tablesorter-noSort",cssIgnoreRow:"tablesorter-ignoreRow",cssIcon:"tablesorter-icon",cssIconNone:"",cssIconAsc:"",cssIconDesc:"",pointerClick:"click",pointerDown:"mousedown",pointerUp:"mouseup",selectorHeaders:"> thead th, > thead td",selectorSort:"th, td",selectorRemove:".remove-me",debug:!1,headerList:[],empties:{},strings:{},parsers:[],globalize:0,imgAttr:0},css:{table:"tablesorter",cssHasChild:"tablesorter-hasChildRow",childRow:"tablesorter-childRow",colgroup:"tablesorter-colgroup",header:"tablesorter-header",headerRow:"tablesorter-headerRow",headerIn:"tablesorter-header-inner",icon:"tablesorter-icon",processing:"tablesorter-processing",sortAsc:"tablesorter-headerAsc",sortDesc:"tablesorter-headerDesc",sortNone:"tablesorter-headerUnSorted"},language:{sortAsc:"Ascending sort applied, ",sortDesc:"Descending sort applied, ",sortNone:"No sort applied, ",sortDisabled:"sorting is disabled",nextAsc:"activate to apply an ascending sort",nextDesc:"activate to apply a descending sort",nextNone:"activate to remove the sort"},regex:{templateContent:/\{content\}/g,templateIcon:/\{icon\}/g,templateName:/\{name\}/i,spaces:/\s+/g,nonWord:/\W/g,formElements:/(input|select|button|textarea)/i,chunk:/(^([+\-]?(?:\d*)(?:\.\d*)?(?:[eE][+\-]?\d+)?)?$|^0x[0-9a-f]+$|\d+)/gi,chunks:/(^\\0|\\0$)/,hex:/^0x[0-9a-f]+$/i,comma:/,/g,digitNonUS:/[\s|\.]/g,digitNegativeTest:/^\s*\([.\d]+\)/,digitNegativeReplace:/^\s*\(([.\d]+)\)/,digitTest:/^[\-+(]?\d+[)]?$/,digitReplace:/[,.'"\s]/g},string:{max:1,min:-1,emptymin:1,emptymax:-1,zero:0,none:0,null:0,top:!0,bottom:!1},keyCodes:{enter:13},dates:{},instanceMethods:{},setup:function(a,d){if(!a||!a.tHead||0===a.tBodies.length||a.hasInitialized===!0)return void(d.debug&&(a.hasInitialized?console.warn("Stopping initialization. Tablesorter has already been initialized"):console.error("Stopping initialization! No table, thead or tbody",a)));var e="",f=b(a),g=b.metadata;a.hasInitialized=!1,a.isProcessing=!0,a.config=d,b.data(a,"tablesorter",d),d.debug&&(console[console.group?"group":"log"]("Initializing tablesorter v"+c.version),b.data(a,"startoveralltimer",new Date)),d.supportsDataObject=function(a){return a[0]=parseInt(a[0],10),a[0]>1||1===a[0]&&parseInt(a[1],10)>=4}(b.fn.jquery.split(".")),d.emptyTo=d.emptyTo.toLowerCase(),d.stringTo=d.stringTo.toLowerCase(),d.last={sortList:[],clickedIndex:-1},/tablesorter\-/.test(f.attr("class"))||(e=""!==d.theme?" tablesorter-"+d.theme:""),d.table=a,d.$table=f.addClass(c.css.table+" "+d.tableClass+e).attr("role","grid"),d.$headers=f.find(d.selectorHeaders),d.namespace?d.namespace="."+d.namespace.replace(c.regex.nonWord,""):d.namespace=".tablesorter"+Math.random().toString(16).slice(2),d.$table.children().children("tr").attr("role","row"),d.$tbodies=f.children("tbody:not(."+d.cssInfoBlock+")").attr({"aria-live":"polite","aria-relevant":"all"}),d.$table.children("caption").length&&(e=d.$table.children("caption")[0],e.id||(e.id=d.namespace.slice(1)+"caption"),d.$table.attr("aria-labelledby",e.id)),d.widgetInit={},d.textExtraction=d.$table.attr("data-text-extraction")||d.textExtraction||"basic",c.buildHeaders(d),c.fixColumnWidth(a),c.addWidgetFromClass(a),c.applyWidgetOptions(a),c.setupParsers(d),d.totalRows=0,c.validateOptions(d),d.delayInit||c.buildCache(d),c.bindEvents(a,d.$headers,!0),c.bindMethods(d),d.supportsDataObject&&"undefined"!=typeof f.data().sortlist?d.sortList=f.data().sortlist:g&&f.metadata()&&f.metadata().sortlist&&(d.sortList=f.metadata().sortlist),c.applyWidget(a,!0),d.sortList.length>0?c.sortOn(d,d.sortList,{},!d.initWidgets):(c.setHeadersCss(d),d.initWidgets&&c.applyWidget(a,!1)),d.showProcessing&&f.unbind("sortBegin"+d.namespace+" sortEnd"+d.namespace).bind("sortBegin"+d.namespace+" sortEnd"+d.namespace,function(b){clearTimeout(d.timerProcessing),c.isProcessing(a),"sortBegin"===b.type&&(d.timerProcessing=setTimeout(function(){c.isProcessing(a,!0)},500))}),a.hasInitialized=!0,a.isProcessing=!1,d.debug&&(console.log("Overall initialization time:"+c.benchmark(b.data(a,"startoveralltimer"))),d.debug&&console.groupEnd&&console.groupEnd()),f.triggerHandler("tablesorter-initialized",a),"function"==typeof d.initialized&&d.initialized(a)},bindMethods:function(a){var d=a.$table,e=a.namespace,f="sortReset update updateRows updateAll updateHeaders addRows updateCell updateComplete sorton appendCache updateCache applyWidgetId applyWidgets refreshWidgets destroy mouseup mouseleave ".split(" ").join(e+" ");d.unbind(f.replace(c.regex.spaces," ")).bind("sortReset"+e,function(a,b){a.stopPropagation(),c.sortReset(this.config,b)}).bind("updateAll"+e,function(a,b,d){a.stopPropagation(),c.updateAll(this.config,b,d)}).bind("update"+e+" updateRows"+e,function(a,b,d){a.stopPropagation(),c.update(this.config,b,d)}).bind("updateHeaders"+e,function(a,b){a.stopPropagation(),c.updateHeaders(this.config,b)}).bind("updateCell"+e,function(a,b,d,e){a.stopPropagation(),c.updateCell(this.config,b,d,e)}).bind("addRows"+e,function(a,b,d,e){a.stopPropagation(),c.addRows(this.config,b,d,e)}).bind("updateComplete"+e,function(){this.isUpdating=!1}).bind("sorton"+e,function(a,b,d,e){a.stopPropagation(),c.sortOn(this.config,b,d,e)}).bind("appendCache"+e,function(a,d,e){a.stopPropagation(),c.appendCache(this.config,e),b.isFunction(d)&&d(this)}).bind("updateCache"+e,function(a,b,d){a.stopPropagation(),c.updateCache(this.config,b,d)}).bind("applyWidgetId"+e,function(a,b){a.stopPropagation(),c.applyWidgetId(this,b)}).bind("applyWidgets"+e,function(a,b){a.stopPropagation(),c.applyWidget(this,b)}).bind("refreshWidgets"+e,function(a,b,d){a.stopPropagation(),c.refreshWidgets(this,b,d)}).bind("removeWidget"+e,function(a,b,d){a.stopPropagation(),c.removeWidget(this,b,d)}).bind("destroy"+e,function(a,b,d){a.stopPropagation(),c.destroy(this,b,d)}).bind("resetToLoadState"+e,function(d){d.stopPropagation(),c.removeWidget(this,!0,!1);var e=b.extend(!0,{},a.originalSettings);a=b.extend(!0,c.defaults,e),a.originalSettings=e,this.hasInitialized=!1,c.setup(this,a)})},bindEvents:function(a,d,e){a=b(a)[0];var f,g=a.config,h=g.namespace,i=null;e!==!0&&(d.addClass(h.slice(1)+"_extra_headers"),f=b.fn.closest?d.closest("table")[0]:d.parents("table")[0],f&&"TABLE"===f.nodeName&&f!==a&&b(f).addClass(h.slice(1)+"_extra_table")),f=(g.pointerDown+" "+g.pointerUp+" "+g.pointerClick+" sort keyup ").replace(c.regex.spaces," ").split(" ").join(h+" "),d.find(g.selectorSort).add(d.filter(g.selectorSort)).unbind(f).bind(f,function(a,e){var f,h,j,k=b(a.target),l=" "+a.type+" ";if(!(1!==(a.which||a.button)&&!l.match(" "+g.pointerClick+" | sort | keyup ")||" keyup "===l&&a.which!==c.keyCodes.enter||l.match(" "+g.pointerClick+" ")&&"undefined"!=typeof a.which||l.match(" "+g.pointerUp+" ")&&i!==a.target&&e!==!0)){if(l.match(" "+g.pointerDown+" "))return i=a.target,j=k.jquery.split("."),void("1"===j[0]&&j[1]<4&&a.preventDefault());if(i=null,c.regex.formElements.test(a.target.nodeName)||k.hasClass(g.cssNoSort)||k.parents("."+g.cssNoSort).length>0||k.parents("button").length>0)return!g.cancelSelection;g.delayInit&&c.isEmptyObject(g.cache)&&c.buildCache(g),f=b.fn.closest?b(this).closest("th, td"):/TH|TD/.test(this.nodeName)?b(this):b(this).parents("th, td"),j=d.index(f),g.last.clickedIndex=j<0?f.attr("data-column"):j,h=g.$headers[g.last.clickedIndex],h&&!h.sortDisabled&&c.initSort(g,h,a)}}),g.cancelSelection&&d.attr("unselectable","on").bind("selectstart",!1).css({"user-select":"none",MozUserSelect:"none"})},buildHeaders:function(a){var d,e,f,g;for(a.headerList=[],a.headerContent=[],a.sortVars=[],a.debug&&(f=new Date),a.columns=c.computeColumnIndex(a.$table.children("thead, tfoot").children("tr")),e=a.cssIcon?'<i class="'+(a.cssIcon===c.css.icon?c.css.icon:a.cssIcon+" "+c.css.icon)+'"></i>':"",a.$headers=b(b.map(a.$table.find(a.selectorHeaders),function(d,f){var g,h,i,j,k,l=b(d);if(!l.parent().hasClass(a.cssIgnoreRow))return g=c.getColumnData(a.table,a.headers,f,!0),a.headerContent[f]=l.html(),""===a.headerTemplate||l.find("."+c.css.headerIn).length||(j=a.headerTemplate.replace(c.regex.templateContent,l.html()).replace(c.regex.templateIcon,l.find("."+c.css.icon).length?"":e),a.onRenderTemplate&&(h=a.onRenderTemplate.apply(l,[f,j]),h&&"string"==typeof h&&(j=h)),l.html('<div class="'+c.css.headerIn+'">'+j+"</div>")),a.onRenderHeader&&a.onRenderHeader.apply(l,[f,a,a.$table]),i=parseInt(l.attr("data-column"),10),d.column=i,k=c.getOrder(c.getData(l,g,"sortInitialOrder")||a.sortInitialOrder),a.sortVars[i]={count:-1,order:k?a.sortReset?[1,0,2]:[1,0]:a.sortReset?[0,1,2]:[0,1],lockedOrder:!1},k=c.getData(l,g,"lockedOrder")||!1,"undefined"!=typeof k&&k!==!1&&(a.sortVars[i].lockedOrder=!0,a.sortVars[i].order=c.getOrder(k)?[1,1]:[0,0]),a.headerList[f]=d,l.addClass(c.css.header+" "+a.cssHeader).parent().addClass(c.css.headerRow+" "+a.cssHeaderRow).attr("role","row"),a.tabIndex&&l.attr("tabindex",0),d})),a.$headerIndexed=[],g=0;g<a.columns;g++)c.isEmptyObject(a.sortVars[g])&&(a.sortVars[g]={}),d=a.$headers.filter('[data-column="'+g+'"]'),a.$headerIndexed[g]=d.length?d.not(".sorter-false").length?d.not(".sorter-false").filter(":last"):d.filter(":last"):b();a.$table.find(a.selectorHeaders).attr({scope:"col",role:"columnheader"}),c.updateHeader(a),a.debug&&(console.log("Built headers:"+c.benchmark(f)),console.log(a.$headers))},addInstanceMethods:function(a){b.extend(c.instanceMethods,a)},setupParsers:function(a,b){var d,e,f,g,h,i,j,k,l,m,n,o,p,q,r=a.table,s=0,t={};if(a.$tbodies=a.$table.children("tbody:not(."+a.cssInfoBlock+")"),p="undefined"==typeof b?a.$tbodies:b,q=p.length,0===q)return a.debug?console.warn("Warning: *Empty table!* Not building a parser cache"):"";for(a.debug&&(o=new Date,console[console.group?"group":"log"]("Detecting parsers for each column")),e={extractors:[],parsers:[]};s<q;){if(d=p[s].rows,d.length)for(h=0,g=a.columns,i=0;i<g;i++){if(j=a.$headerIndexed[h],j&&j.length&&(k=c.getColumnData(r,a.headers,h),n=c.getParserById(c.getData(j,k,"extractor")),m=c.getParserById(c.getData(j,k,"sorter")),l="false"===c.getData(j,k,"parser"),a.empties[h]=(c.getData(j,k,"empty")||a.emptyTo||(a.emptyToBottom?"bottom":"top")).toLowerCase(),a.strings[h]=(c.getData(j,k,"string")||a.stringTo||"max").toLowerCase(),l&&(m=c.getParserById("no-parser")),n||(n=!1),m||(m=c.detectParserForColumn(a,d,-1,h)),a.debug&&(t["("+h+") "+j.text()]={parser:m.id,extractor:n?n.id:"none",string:a.strings[h],empty:a.empties[h]}),e.parsers[h]=m,e.extractors[h]=n,f=j[0].colSpan-1,f>0))for(h+=f,g+=f;f+1>0;)e.parsers[h-f]=m,e.extractors[h-f]=n,f--;h++}s+=e.parsers.length?q:1}a.debug&&(c.isEmptyObject(t)?console.warn("  No parsers detected!"):console[console.table?"table":"log"](t),console.log("Completed detecting parsers"+c.benchmark(o)),console.groupEnd&&console.groupEnd()),a.parsers=e.parsers,a.extractors=e.extractors},addParser:function(a){var b,d=c.parsers.length,e=!0;for(b=0;b<d;b++)c.parsers[b].id.toLowerCase()===a.id.toLowerCase()&&(e=!1);e&&(c.parsers[c.parsers.length]=a)},getParserById:function(a){if("false"==a)return!1;var b,d=c.parsers.length;for(b=0;b<d;b++)if(c.parsers[b].id.toLowerCase()===a.toString().toLowerCase())return c.parsers[b];return!1},detectParserForColumn:function(a,d,e,f){for(var g,h,i,j=c.parsers.length,k=!1,l="",m=!0;""===l&&m;)e++,i=d[e],i&&e<50?i.className.indexOf(c.cssIgnoreRow)<0&&(k=d[e].cells[f],l=c.getElementText(a,k,f),h=b(k),a.debug&&console.log("Checking if value was empty on row "+e+", column: "+f+': "'+l+'"')):m=!1;for(;--j>=0;)if(g=c.parsers[j],g&&"text"!==g.id&&g.is&&g.is(l,a.table,k,h))return g;return c.getParserById("text")},getElementText:function(a,d,e){if(!d)return"";var f,g=a.textExtraction||"",h=d.jquery?d:b(d);return"string"==typeof g?"basic"===g&&"undefined"!=typeof(f=h.attr(a.textAttribute))?b.trim(f):b.trim(d.textContent||h.text()):"function"==typeof g?b.trim(g(h[0],a.table,e)):"function"==typeof(f=c.getColumnData(a.table,g,e))?b.trim(f(h[0],a.table,e)):b.trim(h[0].textContent||h.text())},getParsedText:function(a,b,d,e){"undefined"==typeof e&&(e=c.getElementText(a,b,d));var f=""+e,g=a.parsers[d],h=a.extractors[d];return g&&(h&&"function"==typeof h.format&&(e=h.format(e,a.table,b,d)),f="no-parser"===g.id?"":g.format(""+e,a.table,b,d),a.ignoreCase&&"string"==typeof f&&(f=f.toLowerCase())),f},buildCache:function(a,d,e){var f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,A,B=a.table,C=a.parsers;if(a.$tbodies=a.$table.children("tbody:not(."+a.cssInfoBlock+")"),l="undefined"==typeof e?a.$tbodies:e,a.cache={},a.totalRows=0,!C)return a.debug?console.warn("Warning: *Empty table!* Not building a cache"):"";for(a.debug&&(q=new Date),a.showProcessing&&c.isProcessing(B,!0),k=0;k<l.length;k++){for(u=[],f=a.cache[k]={normalized:[]},r=l[k]&&l[k].rows.length||0,i=0;i<r;++i)if(s={child:[],raw:[]},m=b(l[k].rows[i]),n=[],!m.hasClass(a.selectorRemove.slice(1)))if(m.hasClass(a.cssChildRow)&&0!==i)for(z=f.normalized.length-1,t=f.normalized[z][a.columns],t.$row=t.$row.add(m),m.prev().hasClass(a.cssChildRow)||m.prev().addClass(c.css.cssHasChild),o=m.children("th, td"),z=t.child.length,t.child[z]=[],w=0,y=a.columns,j=0;j<y;j++)p=o[j],p&&(t.child[z][j]=c.getParsedText(a,p,j),v=o[j].colSpan-1,v>0&&(w+=v,y+=v)),w++;else{for(s.$row=m,s.order=i,w=0,y=a.columns,j=0;j<y;++j){if(p=m[0].cells[j],p&&w<a.columns&&(x="undefined"!=typeof C[w],!x&&a.debug&&console.warn("No parser found for row: "+i+", column: "+j+'; cell containing: "'+b(p).text()+'"; does it have a header?'),g=c.getElementText(a,p,w),s.raw[w]=g,h=c.getParsedText(a,p,w,g),n[w]=h,x&&"numeric"===(C[w].type||"").toLowerCase()&&(u[w]=Math.max(Math.abs(h)||0,u[w]||0)),v=p.colSpan-1,v>0)){for(A=0;A<=v;)h=a.duplicateSpan||0===A?g:"string"!=typeof a.textExtraction?c.getElementText(a,p,w+A)||"":"",s.raw[w+A]=h,n[w+A]=h,A++;w+=v,y+=v}w++}n[a.columns]=s,f.normalized[f.normalized.length]=n}f.colMax=u,a.totalRows+=f.normalized.length}if(a.showProcessing&&c.isProcessing(B),a.debug){for(z=Math.min(5,a.cache[0].normalized.length),console[console.group?"group":"log"]("Building cache for "+a.totalRows+" rows (showing "+z+" rows in log) and "+a.columns+" columns"+c.benchmark(q)),g={},j=0;j<a.columns;j++)for(w=0;w<z;w++)g["row: "+w]||(g["row: "+w]={}),g["row: "+w][a.$headerIndexed[j].text()]=a.cache[0].normalized[w][j];console[console.table?"table":"log"](g),console.groupEnd&&console.groupEnd()}b.isFunction(d)&&d(B)},getColumnText:function(a,d,e,f){a=b(a)[0];var g,h,i,j,k,l,m,n,o,p,q="function"==typeof e,r="all"===d,s={raw:[],parsed:[],$cell:[]},t=a.config;if(!c.isEmptyObject(t)){for(k=t.$tbodies.length,g=0;g<k;g++)for(i=t.cache[g].normalized,l=i.length,h=0;h<l;h++)j=i[h],f&&!j[t.columns].$row.is(f)||(p=!0,n=r?j.slice(0,t.columns):j[d],j=j[t.columns],m=r?j.raw:j.raw[d],o=r?j.$row.children():j.$row.children().eq(d),q&&(p=e({tbodyIndex:g,rowIndex:h,parsed:n,raw:m,$row:j.$row,$cell:o})),p!==!1&&(s.parsed[s.parsed.length]=n,s.raw[s.raw.length]=m,s.$cell[s.$cell.length]=o));return s}t.debug&&console.warn("No cache found - aborting getColumnText function!")},setHeadersCss:function(a){var d,e,f,g=a.sortList,h=g.length,i=c.css.sortNone+" "+a.cssNone,j=[c.css.sortAsc+" "+a.cssAsc,c.css.sortDesc+" "+a.cssDesc],k=[a.cssIconAsc,a.cssIconDesc,a.cssIconNone],l=["ascending","descending"],m=a.$table.find("tfoot tr").children("td, th").add(b(a.namespace+"_extra_headers")).removeClass(j.join(" "));for(a.$headers.add(b("thead "+a.namespace+"_extra_headers")).removeClass(j.join(" ")).addClass(i).attr("aria-sort","none").find("."+c.css.icon).removeClass(k.join(" ")).addClass(k[2]),e=0;e<h;e++)if(2!==g[e][1]&&(d=a.$headers.filter(function(b){for(var d=!0,e=a.$headers.eq(b),f=parseInt(e.attr("data-column"),10),g=f+a.$headers[b].colSpan;f<g;f++)d=!!d&&(d||c.isValueInArray(f,a.sortList)>-1);return d}),d=d.not(".sorter-false").filter('[data-column="'+g[e][0]+'"]'+(1===h?":last":"")),d.length)){for(f=0;f<d.length;f++)d[f].sortDisabled||d.eq(f).removeClass(i).addClass(j[g[e][1]]).attr("aria-sort",l[g[e][1]]).find("."+c.css.icon).removeClass(k[2]).addClass(k[g[e][1]]);m.length&&m.filter('[data-column="'+g[e][0]+'"]').removeClass(i).addClass(j[g[e][1]])}for(h=a.$headers.length,e=0;e<h;e++)c.setColumnAriaLabel(a,a.$headers.eq(e))},setColumnAriaLabel:function(a,d,e){if(d.length){var f=parseInt(d.attr("data-column"),10),g=a.sortVars[f],h=d.hasClass(c.css.sortAsc)?"sortAsc":d.hasClass(c.css.sortDesc)?"sortDesc":"sortNone",i=b.trim(d.text())+": "+c.language[h];d.hasClass("sorter-false")||e===!1?i+=c.language.sortDisabled:(h=(g.count+1)%g.order.length,e=g.order[h],i+=c.language[0===e?"nextAsc":1===e?"nextDesc":"nextNone"]),d.attr("aria-label",i)}},updateHeader:function(a){var b,d,e,f,g=a.table,h=a.$headers.length;for(b=0;b<h;b++)e=a.$headers.eq(b),f=c.getColumnData(g,a.headers,b,!0),d="false"===c.getData(e,f,"sorter")||"false"===c.getData(e,f,"parser"),c.setColumnSort(a,e,d)},setColumnSort:function(a,b,c){var d=a.table.id;b[0].sortDisabled=c,b[c?"addClass":"removeClass"]("sorter-false").attr("aria-disabled",""+c),a.tabIndex&&(c?b.removeAttr("tabindex"):b.attr("tabindex","0")),d&&(c?b.removeAttr("aria-controls"):b.attr("aria-controls",d))},updateHeaderSortCount:function(a,d){var e,f,g,h,i,j,k,l,m=d||a.sortList,n=m.length;for(a.sortList=[],h=0;h<n;h++)if(k=m[h],e=parseInt(k[0],10),e<a.columns){switch(a.sortVars[e].order||(l=c.getOrder(a.sortInitialOrder)?a.sortReset?[1,0,2]:[1,0]:a.sortReset?[0,1,2]:[0,1],a.sortVars[e].order=l,a.sortVars[e].count=0),l=a.sortVars[e].order,f=(""+k[1]).match(/^(1|d|s|o|n)/),f=f?f[0]:""){case"1":case"d":f=1;break;case"s":f=i||0;break;case"o":j=l[(i||0)%l.length],f=0===j?1:1===j?0:2;break;case"n":f=l[++a.sortVars[e].count%l.length];break;default:f=0}i=0===h?f:i,g=[e,parseInt(f,10)||0],a.sortList[a.sortList.length]=g,f=b.inArray(g[1],l),a.sortVars[e].count=f>=0?f:g[1]%l.length}},updateAll:function(a,b,d){var e=a.table;e.isUpdating=!0,c.refreshWidgets(e,!0,!0),c.buildHeaders(a),c.bindEvents(e,a.$headers,!0),c.bindMethods(a),c.commonUpdate(a,b,d)},update:function(a,b,d){var e=a.table;e.isUpdating=!0,c.updateHeader(a),c.commonUpdate(a,b,d)},updateHeaders:function(a,b){a.table.isUpdating=!0,c.buildHeaders(a),c.bindEvents(a.table,a.$headers,!0),c.resortComplete(a,b)},updateCell:function(a,d,e,f){if(c.isEmptyObject(a.cache))return c.updateHeader(a),void c.commonUpdate(a,e,f);a.table.isUpdating=!0,a.$table.find(a.selectorRemove).remove();var g,h,i,j,k,l,m=a.$tbodies,n=b(d),o=m.index(b.fn.closest?n.closest("tbody"):n.parents("tbody").filter(":first")),p=a.cache[o],q=b.fn.closest?n.closest("tr"):n.parents("tr").filter(":first");if(d=n[0],m.length&&o>=0){if(i=m.eq(o).find("tr").index(q),k=p.normalized[i],l=q[0].cells.length,l!==a.columns)for(j=0,g=!1,h=0;h<l;h++)g||q[0].cells[h]===d?g=!0:j+=q[0].cells[h].colSpan;else j=n.index();g=c.getElementText(a,d,j),k[a.columns].raw[j]=g,g=c.getParsedText(a,d,j,g),k[j]=g,k[a.columns].$row=q,"numeric"===(a.parsers[j].type||"").toLowerCase()&&(p.colMax[j]=Math.max(Math.abs(g)||0,p.colMax[j]||0)),g="undefined"!==e?e:a.resort,g!==!1?c.checkResort(a,g,f):c.resortComplete(a,f)}else a.debug&&console.error("updateCell aborted, tbody missing or not within the indicated table"),a.table.isUpdating=!1},addRows:function(d,e,f,g){var h,i,j,k,l,m,n,o,p,q,r,s,t,u="string"==typeof e&&1===d.$tbodies.length&&/<tr/.test(e||""),v=d.table;if(u)e=b(e),d.$tbodies.append(e);else if(!(e&&e instanceof a&&(b.fn.closest?e.closest("table")[0]:e.parents("table")[0])===d.table))return d.debug&&console.error("addRows method requires (1) a jQuery selector reference to rows that have already been added to the table, or (2) row HTML string to be added to a table with only one tbody"),!1;if(v.isUpdating=!0,c.isEmptyObject(d.cache))c.updateHeader(d),c.commonUpdate(d,f,g);else{for(l=e.filter("tr").attr("role","row").length,j=d.$tbodies.index(e.parents("tbody").filter(":first")),d.parsers&&d.parsers.length||c.setupParsers(d),k=0;k<l;k++){for(p=0,n=e[k].cells.length,o=d.cache[j].normalized.length,r=[],q={child:[],raw:[],$row:e.eq(k),order:o},m=0;m<n;m++)s=e[k].cells[m],h=c.getElementText(d,s,p),q.raw[p]=h,i=c.getParsedText(d,s,p,h),r[p]=i,"numeric"===(d.parsers[p].type||"").toLowerCase()&&(d.cache[j].colMax[p]=Math.max(Math.abs(i)||0,d.cache[j].colMax[p]||0)),t=s.colSpan-1,t>0&&(p+=t),p++;r[d.columns]=q,d.cache[j].normalized[o]=r}c.checkResort(d,f,g)}},updateCache:function(a,b,d){a.parsers&&a.parsers.length||c.setupParsers(a,d),c.buildCache(a,b,d)},appendCache:function(a,b){var d,e,f,g,h,i,j,k=a.table,l=a.widgetOptions,m=a.$tbodies,n=[],o=a.cache;if(c.isEmptyObject(o))return a.appender?a.appender(k,n):k.isUpdating?a.$table.triggerHandler("updateComplete",k):"";for(a.debug&&(j=new Date),i=0;i<m.length;i++)if(f=m.eq(i),f.length){for(g=c.processTbody(k,f,!0),d=o[i].normalized,e=d.length,h=0;h<e;h++)n[n.length]=d[h][a.columns].$row,a.appender&&(!a.pager||a.pager.removeRows&&l.pager_removeRows||a.pager.ajax)||g.append(d[h][a.columns].$row);c.processTbody(k,g,!1)}a.appender&&a.appender(k,n),a.debug&&console.log("Rebuilt table"+c.benchmark(j)),b||a.appender||c.applyWidget(k),k.isUpdating&&a.$table.triggerHandler("updateComplete",k)},commonUpdate:function(a,b,d){a.$table.find(a.selectorRemove).remove(),c.setupParsers(a),c.buildCache(a),c.checkResort(a,b,d)},initSort:function(a,d,e){if(a.table.isUpdating)return setTimeout(function(){c.initSort(a,d,e)},50);var f,g,h,i,j,k,l,m=!e[a.sortMultiSortKey],n=a.table,o=a.$headers.length,p=parseInt(b(d).attr("data-column"),10),q=a.sortVars[p].order;if(a.$table.triggerHandler("sortStart",n),k=(a.sortVars[p].count+1)%q.length,a.sortVars[p].count=e[a.sortResetKey]?2:k,a.sortRestart)for(h=0;h<o;h++)l=a.$headers.eq(h),k=parseInt(l.attr("data-column"),10),p!==k&&(m||l.hasClass(c.css.sortNone))&&(a.sortVars[k].count=-1);if(m){if(a.sortList=[],a.last.sortList=[],null!==a.sortForce)for(f=a.sortForce,g=0;g<f.length;g++)f[g][0]!==p&&(a.sortList[a.sortList.length]=f[g]);if(i=q[a.sortVars[p].count],i<2&&(a.sortList[a.sortList.length]=[p,i],d.colSpan>1))for(g=1;g<d.colSpan;g++)a.sortList[a.sortList.length]=[p+g,i],a.sortVars[p+g].count=b.inArray(i,q)}else if(a.sortList=b.extend([],a.last.sortList),c.isValueInArray(p,a.sortList)>=0)for(g=0;g<a.sortList.length;g++)k=a.sortList[g],k[0]===p&&(k[1]=q[a.sortVars[p].count],2===k[1]&&(a.sortList.splice(g,1),a.sortVars[p].count=-1));else if(i=q[a.sortVars[p].count],i<2&&(a.sortList[a.sortList.length]=[p,i],d.colSpan>1))for(g=1;g<d.colSpan;g++)a.sortList[a.sortList.length]=[p+g,i],a.sortVars[p+g].count=b.inArray(i,q);if(a.last.sortList=b.extend([],a.sortList),a.sortList.length&&a.sortAppend&&(f=b.isArray(a.sortAppend)?a.sortAppend:a.sortAppend[a.sortList[0][0]],!c.isEmptyObject(f)))for(g=0;g<f.length;g++)if(f[g][0]!==p&&c.isValueInArray(f[g][0],a.sortList)<0){if(i=f[g][1],j=(""+i).match(/^(a|d|s|o|n)/))switch(k=a.sortList[0][1],j[0]){case"d":i=1;break;case"s":i=k;break;case"o":i=0===k?1:0;break;case"n":i=(k+1)%q.length;break;default:i=0}a.sortList[a.sortList.length]=[f[g][0],i]}a.$table.triggerHandler("sortBegin",n),setTimeout(function(){c.setHeadersCss(a),c.multisort(a),c.appendCache(a),a.$table.triggerHandler("sortBeforeEnd",n),a.$table.triggerHandler("sortEnd",n)},1)},multisort:function(a){var b,d,e,f,g,h=a.table,i=[],j=0,k=a.textSorter||"",l=a.sortList,m=l.length,n=a.$tbodies.length;if(!a.serverSideSorting&&!c.isEmptyObject(a.cache)){if(a.debug&&(d=new Date),"object"==typeof k)for(e=a.columns;e--;)g=c.getColumnData(h,k,e),"function"==typeof g&&(i[e]=g);for(b=0;b<n;b++)e=a.cache[b].colMax,f=a.cache[b].normalized,f.sort(function(b,d){var f,g,n,o,p,q,r;for(f=0;f<m;f++){if(n=l[f][0],o=l[f][1],j=0===o,a.sortStable&&b[n]===d[n]&&1===m)return b[a.columns].order-d[a.columns].order;if(g=/n/i.test(c.getSortType(a.parsers,n)),g&&a.strings[n]?(g="boolean"==typeof c.string[a.strings[n]]?(j?1:-1)*(c.string[a.strings[n]]?-1:1):a.strings[n]?c.string[a.strings[n]]||0:0,p=a.numberSorter?a.numberSorter(b[n],d[n],j,e[n],h):c["sortNumeric"+(j?"Asc":"Desc")](b[n],d[n],g,e[n],n,a)):(q=j?b:d,r=j?d:b,p="function"==typeof k?k(q[n],r[n],j,n,h):"function"==typeof i[n]?i[n](q[n],r[n],j,n,h):c["sortNatural"+(j?"Asc":"Desc")](b[n],d[n],n,a)),p)return p}return b[a.columns].order-d[a.columns].order});a.debug&&console.log("Applying sort "+l.toString()+c.benchmark(d))}},resortComplete:function(a,c){a.table.isUpdating&&a.$table.triggerHandler("updateComplete",a.table),b.isFunction(c)&&c(a.table)},checkResort:function(a,d,e){var f=b.isArray(d)?d:a.sortList,g="undefined"==typeof d?a.resort:d;g===!1||a.serverSideSorting||a.table.isProcessing?(c.resortComplete(a,e),c.applyWidget(a.table,!1)):f.length?c.sortOn(a,f,function(){c.resortComplete(a,e)},!0):c.sortReset(a,function(){c.resortComplete(a,e),c.applyWidget(a.table,!1)})},sortOn:function(a,d,e,f){var g=a.table;a.$table.triggerHandler("sortStart",g),c.updateHeaderSortCount(a,d),c.setHeadersCss(a),a.delayInit&&c.isEmptyObject(a.cache)&&c.buildCache(a),a.$table.triggerHandler("sortBegin",g),c.multisort(a),c.appendCache(a,f),a.$table.triggerHandler("sortBeforeEnd",g),a.$table.triggerHandler("sortEnd",g),c.applyWidget(g),b.isFunction(e)&&e(g)},sortReset:function(a,d){a.sortList=[],c.setHeadersCss(a),c.multisort(a),c.appendCache(a),b.isFunction(d)&&d(a.table)},getSortType:function(a,b){return a&&a[b]?a[b].type||"":""},getOrder:function(a){return/^d/i.test(a)||1===a},sortNatural:function(a,b){if(a===b)return 0;var d,e,f,g,h,i,j=c.regex;if(j.hex.test(b)){if(d=parseInt((a||"").match(j.hex),16),e=parseInt((b||"").match(j.hex),16),d<e)return-1;if(d>e)return 1}for(d=(a||"").replace(j.chunk,"\\0$1\\0").replace(j.chunks,"").split("\\0"),e=(b||"").replace(j.chunk,"\\0$1\\0").replace(j.chunks,"").split("\\0"),i=Math.max(d.length,e.length),h=0;h<i;h++){if(f=isNaN(d[h])?d[h]||0:parseFloat(d[h])||0,g=isNaN(e[h])?e[h]||0:parseFloat(e[h])||0,isNaN(f)!==isNaN(g))return isNaN(f)?1:-1;if(typeof f!=typeof g&&(f+="",g+=""),f<g)return-1;if(f>g)return 1}return 0},sortNaturalAsc:function(a,b,d,e){if(a===b)return 0;var f=c.string[e.empties[d]||e.emptyTo];return""===a&&0!==f?"boolean"==typeof f?f?-1:1:-f||-1:""===b&&0!==f?"boolean"==typeof f?f?1:-1:f||1:c.sortNatural(a,b)},sortNaturalDesc:function(a,b,d,e){if(a===b)return 0;var f=c.string[e.empties[d]||e.emptyTo];return""===a&&0!==f?"boolean"==typeof f?f?-1:1:f||1:""===b&&0!==f?"boolean"==typeof f?f?1:-1:-f||-1:c.sortNatural(b,a)},sortText:function(a,b){return a>b?1:a<b?-1:0},getTextValue:function(a,b,c){if(c){var d,e=a?a.length:0,f=c+b;for(d=0;d<e;d++)f+=a.charCodeAt(d);return b*f}return 0},sortNumericAsc:function(a,b,d,e,f,g){if(a===b)return 0;var h=c.string[g.empties[f]||g.emptyTo];return""===a&&0!==h?"boolean"==typeof h?h?-1:1:-h||-1:""===b&&0!==h?"boolean"==typeof h?h?1:-1:h||1:(isNaN(a)&&(a=c.getTextValue(a,d,e)),isNaN(b)&&(b=c.getTextValue(b,d,e)),a-b)},sortNumericDesc:function(a,b,d,e,f,g){if(a===b)return 0;var h=c.string[g.empties[f]||g.emptyTo];return""===a&&0!==h?"boolean"==typeof h?h?-1:1:h||1:""===b&&0!==h?"boolean"==typeof h?h?1:-1:-h||-1:(isNaN(a)&&(a=c.getTextValue(a,d,e)),isNaN(b)&&(b=c.getTextValue(b,d,e)),b-a)},sortNumeric:function(a,b){return a-b},addWidget:function(a){a.id&&!c.isEmptyObject(c.getWidgetById(a.id))&&console.warn('"'+a.id+'" widget was loaded more than once!'),c.widgets[c.widgets.length]=a},hasWidget:function(a,c){return a=b(a),a.length&&a[0].config&&a[0].config.widgetInit[c]||!1},getWidgetById:function(a){var b,d,e=c.widgets.length;for(b=0;b<e;b++)if(d=c.widgets[b],d&&d.id&&d.id.toLowerCase()===a.toLowerCase())return d},applyWidgetOptions:function(a){var d,e,f=a.config,g=f.widgets.length;if(g)for(d=0;d<g;d++)e=c.getWidgetById(f.widgets[d]),e&&e.options&&(f.widgetOptions=b.extend(!0,{},e.options,f.widgetOptions),b.extend(!0,c.defaults.widgetOptions,e.options))},addWidgetFromClass:function(a){var b,d,e=a.config,f="^"+e.widgetClass.replace(c.regex.templateName,"(\\S+)+")+"$",g=new RegExp(f,"g"),h=(a.className||"").split(c.regex.spaces);if(h.length)for(b=h.length,d=0;d<b;d++)h[d].match(g)&&(e.widgets[e.widgets.length]=h[d].replace(g,"$1"))},applyWidgetId:function(a,d,e){a=b(a)[0];var f,g,h,i=a.config,j=i.widgetOptions,k=c.getWidgetById(d);k&&(h=k.id,f=!1,b.inArray(h,i.widgets)<0&&(i.widgets[i.widgets.length]=h),i.debug&&(g=new Date),!e&&i.widgetInit[h]||(i.widgetInit[h]=!0,a.hasInitialized&&c.applyWidgetOptions(a),"function"==typeof k.init&&(f=!0,i.debug&&console[console.group?"group":"log"]("Initializing "+h+" widget"),k.init(a,k,i,j))),e||"function"!=typeof k.format||(f=!0,i.debug&&console[console.group?"group":"log"]("Updating "+h+" widget"),k.format(a,i,j,!1)),i.debug&&f&&(console.log("Completed "+(e?"initializing ":"applying ")+h+" widget"+c.benchmark(g)),console.groupEnd&&console.groupEnd()))},applyWidget:function(a,d,e){a=b(a)[0];var f,g,h,i,j,k=a.config,l=[];if(d===!1||!a.hasInitialized||!a.isApplyingWidgets&&!a.isUpdating){if(k.debug&&(j=new Date),c.addWidgetFromClass(a),clearTimeout(k.timerReady),k.widgets.length){for(a.isApplyingWidgets=!0,k.widgets=b.grep(k.widgets,function(a,c){return b.inArray(a,k.widgets)===c}),h=k.widgets||[],g=h.length,f=0;f<g;f++)i=c.getWidgetById(h[f]),i&&i.id?(i.priority||(i.priority=10),l[f]=i):k.debug&&console.warn('"'+h[f]+'" widget code does not exist!');for(l.sort(function(a,b){return a.priority<b.priority?-1:a.priority===b.priority?0:1}),g=l.length,k.debug&&console[console.group?"group":"log"]("Start "+(d?"initializing":"applying")+" widgets"),f=0;f<g;f++)i=l[f],i&&i.id&&c.applyWidgetId(a,i.id,d);k.debug&&console.groupEnd&&console.groupEnd(),d||"function"!=typeof e||e(a)}k.timerReady=setTimeout(function(){a.isApplyingWidgets=!1,b.data(a,"lastWidgetApplication",new Date),k.$table.triggerHandler("tablesorter-ready")},10),k.debug&&(i=k.widgets.length,console.log("Completed "+(d===!0?"initializing ":"applying ")+i+" widget"+(1!==i?"s":"")+c.benchmark(j)))}},removeWidget:function(a,d,e){a=b(a)[0];var f,g,h,i,j=a.config;if(d===!0)for(d=[],i=c.widgets.length,h=0;h<i;h++)g=c.widgets[h],g&&g.id&&(d[d.length]=g.id);else d=(b.isArray(d)?d.join(","):d||"").toLowerCase().split(/[\s,]+/);for(i=d.length,f=0;f<i;f++)g=c.getWidgetById(d[f]),h=b.inArray(d[f],j.widgets),h>=0&&e!==!0&&j.widgets.splice(h,1),g&&g.remove&&(j.debug&&console.log((e?"Refreshing":"Removing")+' "'+d[f]+'" widget'),g.remove(a,j,j.widgetOptions,e),j.widgetInit[d[f]]=!1)},refreshWidgets:function(a,d,e){a=b(a)[0];var f,g,h=a.config,i=h.widgets,j=c.widgets,k=j.length,l=[],m=function(a){b(a).triggerHandler("refreshComplete")};for(f=0;f<k;f++)g=j[f],g&&g.id&&(d||b.inArray(g.id,i)<0)&&(l[l.length]=g.id);c.removeWidget(a,l.join(","),!0),e!==!0?(c.applyWidget(a,d||!1,m),d&&c.applyWidget(a,!1,m)):m(a)},benchmark:function(a){return" ("+((new Date).getTime()-a.getTime())+" ms)"},log:function(){console.log(arguments)},isEmptyObject:function(a){for(var b in a)return!1;return!0},isValueInArray:function(a,b){var c,d=b&&b.length||0;for(c=0;c<d;c++)if(b[c][0]===a)return c;return-1},formatFloat:function(a,d){if("string"!=typeof a||""===a)return a;var e,f=d&&d.config?d.config.usNumberFormat!==!1:"undefined"==typeof d||d;
return a=f?a.replace(c.regex.comma,""):a.replace(c.regex.digitNonUS,"").replace(c.regex.comma,"."),c.regex.digitNegativeTest.test(a)&&(a=a.replace(c.regex.digitNegativeReplace,"-$1")),e=parseFloat(a),isNaN(e)?b.trim(a):e},isDigit:function(a){return isNaN(a)?c.regex.digitTest.test(a.toString().replace(c.regex.digitReplace,"")):""!==a},computeColumnIndex:function(a,c){var d,e,f,g,h,i,j,k,l,m,n=c&&c.columns||0,o=[],p=new Array(n);for(d=0;d<a.length;d++)for(i=a[d].cells,e=0;e<i.length;e++){for(h=i[e],j=h.parentNode.rowIndex,k=h.rowSpan||1,l=h.colSpan||1,"undefined"==typeof o[j]&&(o[j]=[]),f=0;f<o[j].length+1;f++)if("undefined"==typeof o[j][f]){m=f;break}for(n&&h.cellIndex===m||(h.setAttribute?h.setAttribute("data-column",m):b(h).attr("data-column",m)),f=j;f<j+k;f++)for("undefined"==typeof o[f]&&(o[f]=[]),p=o[f],g=m;g<m+l;g++)p[g]="x"}return p.length},fixColumnWidth:function(a){a=b(a)[0];var d,e,f,g,h,i=a.config,j=i.$table.children("colgroup");if(j.length&&j.hasClass(c.css.colgroup)&&j.remove(),i.widthFixed&&0===i.$table.children("colgroup").length){for(j=b('<colgroup class="'+c.css.colgroup+'">'),d=i.$table.width(),f=i.$tbodies.find("tr:first").children(":visible"),g=f.length,h=0;h<g;h++)e=parseInt(f.eq(h).width()/d*1e3,10)/10+"%",j.append(b("<col>").css("width",e));i.$table.prepend(j)}},getData:function(a,c,d){var e,f,g="",h=b(a);return h.length?(e=!!b.metadata&&h.metadata(),f=" "+(h.attr("class")||""),"undefined"!=typeof h.data(d)||"undefined"!=typeof h.data(d.toLowerCase())?g+=h.data(d)||h.data(d.toLowerCase()):e&&"undefined"!=typeof e[d]?g+=e[d]:c&&"undefined"!=typeof c[d]?g+=c[d]:" "!==f&&f.match(" "+d+"-")&&(g=f.match(new RegExp("\\s"+d+"-([\\w-]+)"))[1]||""),b.trim(g)):""},getColumnData:function(a,c,d,e,f){if("object"!=typeof c||null===c)return c;a=b(a)[0];var g,h,i=a.config,j=f||i.$headers,k=i.$headerIndexed&&i.$headerIndexed[d]||j.filter('[data-column="'+d+'"]:last');if("undefined"!=typeof c[d])return e?c[d]:c[j.index(k)];for(h in c)if("string"==typeof h&&(g=k.filter(h).add(k.find(h)),g.length))return c[h]},isProcessing:function(a,d,e){a=b(a);var f=a[0].config,g=e||a.find("."+c.css.header);d?("undefined"!=typeof e&&f.sortList.length>0&&(g=g.filter(function(){return!this.sortDisabled&&c.isValueInArray(parseFloat(b(this).attr("data-column")),f.sortList)>=0})),a.add(g).addClass(c.css.processing+" "+f.cssProcessing)):a.add(g).removeClass(c.css.processing+" "+f.cssProcessing)},processTbody:function(a,c,d){if(a=b(a)[0],d)return a.isProcessing=!0,c.before('<colgroup class="tablesorter-savemyplace"/>'),b.fn.detach?c.detach():c.remove();var e=b(a).find("colgroup.tablesorter-savemyplace");c.insertAfter(e),e.remove(),a.isProcessing=!1},clearTableBody:function(a){b(a)[0].config.$tbodies.children().detach()},characterEquivalents:{a:"áàâãäąå",A:"ÁÀÂÃÄĄÅ",c:"çćč",C:"ÇĆČ",e:"éèêëěę",E:"ÉÈÊËĚĘ",i:"íìİîïı",I:"ÍÌİÎÏ",o:"óòôõöō",O:"ÓÒÔÕÖŌ",ss:"ß",SS:"ẞ",u:"úùûüů",U:"ÚÙÛÜŮ"},replaceAccents:function(a){var b,d="[",e=c.characterEquivalents;if(!c.characterRegex){c.characterRegexArray={};for(b in e)"string"==typeof b&&(d+=e[b],c.characterRegexArray[b]=new RegExp("["+e[b]+"]","g"));c.characterRegex=new RegExp(d+"]")}if(c.characterRegex.test(a))for(b in e)"string"==typeof b&&(a=a.replace(c.characterRegexArray[b],b));return a},validateOptions:function(a){var d,e,f,g,h="headers sortForce sortList sortAppend widgets".split(" "),i=a.originalSettings;if(i){a.debug&&(g=new Date);for(d in i)if(f=typeof c.defaults[d],"undefined"===f)console.warn('Tablesorter Warning! "table.config.'+d+'" option not recognized');else if("object"===f)for(e in i[d])f=c.defaults[d]&&typeof c.defaults[d][e],b.inArray(d,h)<0&&"undefined"===f&&console.warn('Tablesorter Warning! "table.config.'+d+"."+e+'" option not recognized');a.debug&&console.log("validate options time:"+c.benchmark(g))}},restoreHeaders:function(a){var d,e,f=b(a)[0].config,g=f.$table.find(f.selectorHeaders),h=g.length;for(d=0;d<h;d++)e=g.eq(d),e.find("."+c.css.headerIn).length&&e.html(f.headerContent[d])},destroy:function(a,d,e){if(a=b(a)[0],a.hasInitialized){c.removeWidget(a,!0,!1);var f,g=b(a),h=a.config,i=h.debug,j=g.find("thead:first"),k=j.find("tr."+c.css.headerRow).removeClass(c.css.headerRow+" "+h.cssHeaderRow),l=g.find("tfoot:first > tr").children("th, td");d===!1&&b.inArray("uitheme",h.widgets)>=0&&(g.triggerHandler("applyWidgetId",["uitheme"]),g.triggerHandler("applyWidgetId",["zebra"])),j.find("tr").not(k).remove(),f="sortReset update updateRows updateAll updateHeaders updateCell addRows updateComplete sorton appendCache updateCache applyWidgetId applyWidgets refreshWidgets removeWidget destroy mouseup mouseleave "+"keypress sortBegin sortEnd resetToLoadState ".split(" ").join(h.namespace+" "),g.removeData("tablesorter").unbind(f.replace(c.regex.spaces," ")),h.$headers.add(l).removeClass([c.css.header,h.cssHeader,h.cssAsc,h.cssDesc,c.css.sortAsc,c.css.sortDesc,c.css.sortNone].join(" ")).removeAttr("data-column").removeAttr("aria-label").attr("aria-disabled","true"),k.find(h.selectorSort).unbind("mousedown mouseup keypress ".split(" ").join(h.namespace+" ").replace(c.regex.spaces," ")),c.restoreHeaders(a),g.toggleClass(c.css.table+" "+h.tableClass+" tablesorter-"+h.theme,d===!1),a.hasInitialized=!1,delete a.config.cache,"function"==typeof e&&e(a),i&&console.log("tablesorter has been removed")}}};b.fn.tablesorter=function(a){return this.each(function(){var d=this,e=b.extend(!0,{},c.defaults,a,c.instanceMethods);e.originalSettings=a,!d.hasInitialized&&c.buildTable&&"TABLE"!==this.nodeName?c.buildTable(d,e):c.setup(d,e)})},window.console&&window.console.log||(c.logs=[],console={},console.log=console.warn=console.error=console.table=function(){var a=arguments.length>1?arguments:arguments[0];c.logs[c.logs.length]={date:Date.now(),log:a}}),c.addParser({id:"no-parser",is:function(){return!1},format:function(){return""},type:"text"}),c.addParser({id:"text",is:function(){return!0},format:function(a,d){var e=d.config;return a&&(a=b.trim(e.ignoreCase?a.toLocaleLowerCase():a),a=e.sortLocaleCompare?c.replaceAccents(a):a),a},type:"text"}),c.regex.nondigit=/[^\w,. \-()]/g,c.addParser({id:"digit",is:function(a){return c.isDigit(a)},format:function(a,d){var e=c.formatFloat((a||"").replace(c.regex.nondigit,""),d);return a&&"number"==typeof e?e:a?b.trim(a&&d.config.ignoreCase?a.toLocaleLowerCase():a):a},type:"numeric"}),c.regex.currencyReplace=/[+\-,. ]/g,c.regex.currencyTest=/^\(?\d+[\u00a3$\u20ac\u00a4\u00a5\u00a2?.]|[\u00a3$\u20ac\u00a4\u00a5\u00a2?.]\d+\)?$/,c.addParser({id:"currency",is:function(a){return a=(a||"").replace(c.regex.currencyReplace,""),c.regex.currencyTest.test(a)},format:function(a,d){var e=c.formatFloat((a||"").replace(c.regex.nondigit,""),d);return a&&"number"==typeof e?e:a?b.trim(a&&d.config.ignoreCase?a.toLocaleLowerCase():a):a},type:"numeric"}),c.regex.urlProtocolTest=/^(https?|ftp|file):\/\//,c.regex.urlProtocolReplace=/(https?|ftp|file):\/\/(www\.)?/,c.addParser({id:"url",is:function(a){return c.regex.urlProtocolTest.test(a)},format:function(a){return a?b.trim(a.replace(c.regex.urlProtocolReplace,"")):a},type:"text"}),c.regex.dash=/-/g,c.regex.isoDate=/^\d{4}[\/\-]\d{1,2}[\/\-]\d{1,2}/,c.addParser({id:"isoDate",is:function(a){return c.regex.isoDate.test(a)},format:function(a,b){var d=a?new Date(a.replace(c.regex.dash,"/")):a;return d instanceof Date&&isFinite(d)?d.getTime():a},type:"numeric"}),c.regex.percent=/%/g,c.regex.percentTest=/(\d\s*?%|%\s*?\d)/,c.addParser({id:"percent",is:function(a){return c.regex.percentTest.test(a)&&a.length<15},format:function(a,b){return a?c.formatFloat(a.replace(c.regex.percent,""),b):a},type:"numeric"}),c.addParser({id:"image",is:function(a,b,c,d){return d.find("img").length>0},format:function(a,c,d){return b(d).find("img").attr(c.config.imgAttr||"alt")||a},parsed:!0,type:"text"}),c.regex.dateReplace=/(\S)([AP]M)$/i,c.regex.usLongDateTest1=/^[A-Z]{3,10}\.?\s+\d{1,2},?\s+(\d{4})(\s+\d{1,2}:\d{2}(:\d{2})?(\s+[AP]M)?)?$/i,c.regex.usLongDateTest2=/^\d{1,2}\s+[A-Z]{3,10}\s+\d{4}/i,c.addParser({id:"usLongDate",is:function(a){return c.regex.usLongDateTest1.test(a)||c.regex.usLongDateTest2.test(a)},format:function(a,b){var d=a?new Date(a.replace(c.regex.dateReplace,"$1 $2")):a;return d instanceof Date&&isFinite(d)?d.getTime():a},type:"numeric"}),c.regex.shortDateTest=/(^\d{1,2}[\/\s]\d{1,2}[\/\s]\d{4})|(^\d{4}[\/\s]\d{1,2}[\/\s]\d{1,2})/,c.regex.shortDateReplace=/[\-.,]/g,c.regex.shortDateXXY=/(\d{1,2})[\/\s](\d{1,2})[\/\s](\d{4})/,c.regex.shortDateYMD=/(\d{4})[\/\s](\d{1,2})[\/\s](\d{1,2})/,c.convertFormat=function(a,b){a=(a||"").replace(c.regex.spaces," ").replace(c.regex.shortDateReplace,"/"),"mmddyyyy"===b?a=a.replace(c.regex.shortDateXXY,"$3/$1/$2"):"ddmmyyyy"===b?a=a.replace(c.regex.shortDateXXY,"$3/$2/$1"):"yyyymmdd"===b&&(a=a.replace(c.regex.shortDateYMD,"$1/$2/$3"));var d=new Date(a);return d instanceof Date&&isFinite(d)?d.getTime():""},c.addParser({id:"shortDate",is:function(a){return a=(a||"").replace(c.regex.spaces," ").replace(c.regex.shortDateReplace,"/"),c.regex.shortDateTest.test(a)},format:function(a,b,d,e){if(a){var f=b.config,g=f.$headerIndexed[e],h=g.length&&g.data("dateFormat")||c.getData(g,c.getColumnData(b,f.headers,e),"dateFormat")||f.dateFormat;return g.length&&g.data("dateFormat",h),c.convertFormat(a,h)||a}return a},type:"numeric"}),c.regex.timeTest=/^(0?[1-9]|1[0-2]):([0-5]\d)(\s[AP]M)$|^((?:[01]\d|[2][0-4]):[0-5]\d)$/i,c.regex.timeMatch=/(0?[1-9]|1[0-2]):([0-5]\d)(\s[AP]M)|((?:[01]\d|[2][0-4]):[0-5]\d)/i,c.addParser({id:"time",is:function(a){return c.regex.timeTest.test(a)},format:function(a,b){var d,e=(a||"").match(c.regex.timeMatch),f=new Date(a),g=a&&(null!==e?e[0]:"00:00 AM"),h=g?new Date("2000/01/01 "+g.replace(c.regex.dateReplace,"$1 $2")):g;return h instanceof Date&&isFinite(h)?(d=f instanceof Date&&isFinite(f)?f.getTime():0,d?parseFloat(h.getTime()+"."+f.getTime()):h.getTime()):a},type:"numeric"}),c.addParser({id:"metadata",is:function(){return!1},format:function(a,c,d){var e=c.config,f=e.parserMetadataName?e.parserMetadataName:"sortValue";return b(d).metadata()[f]},type:"numeric"}),c.addWidget({id:"zebra",priority:90,format:function(a,c,d){var e,f,g,h,i,j,k,l=new RegExp(c.cssChildRow,"i"),m=c.$tbodies.add(b(c.namespace+"_extra_table").children("tbody:not(."+c.cssInfoBlock+")"));for(i=0;i<m.length;i++)for(g=0,e=m.eq(i).children("tr:visible").not(c.selectorRemove),k=e.length,j=0;j<k;j++)f=e.eq(j),l.test(f[0].className)||g++,h=g%2===0,f.removeClass(d.zebra[h?1:0]).addClass(d.zebra[h?0:1])},remove:function(a,b,d,e){if(!e){var f,g,h=b.$tbodies,i=(d.zebra||["even","odd"]).join(" ");for(f=0;f<h.length;f++)g=c.processTbody(a,h.eq(f),!0),g.children().removeClass(i),c.processTbody(a,g,!1)}}})}(a),a.tablesorter});

/*! tablesorter (FORK) - updated 01-06-2017 (v2.28.4)*/
!function(a){"function"==typeof define&&define.amd?define(["jquery"],a):"object"==typeof module&&"object"==typeof module.exports?module.exports=a(require("jquery")):a(jQuery)}(function(a){return function(a,b,c){"use strict";var d=a.tablesorter||{};d.storage=function(e,f,g,h){e=a(e)[0];var i,j,k,l=!1,m={},n=e.config,o=n&&n.widgetOptions,p=h&&h.useSessionStorage||o&&o.storage_useSessionStorage?"sessionStorage":"localStorage",q=a(e),r=h&&h.id||q.attr(h&&h.group||o&&o.storage_group||"data-table-group")||o&&o.storage_tableId||e.id||a(".tablesorter").index(q),s=h&&h.url||q.attr(h&&h.page||o&&o.storage_page||"data-table-page")||o&&o.storage_fixedUrl||n&&n.fixedUrl||b.location.pathname;if(a.extend(!0,d.defaults,{fixedUrl:"",widgetOptions:{storage_fixedUrl:"",storage_group:"",storage_page:"",storage_tableId:"",storage_useSessionStorage:""}}),p in b)try{b[p].setItem("_tmptest","temp"),l=!0,b[p].removeItem("_tmptest")}catch(a){n&&n.debug&&console.warn(p+" is not supported in this browser")}return a.parseJSON&&(l?m=a.parseJSON(b[p][f]||"null")||{}:(j=c.cookie.split(/[;\s|=]/),i=a.inArray(f,j)+1,m=0!==i?a.parseJSON(j[i]||"null")||{}:{})),"undefined"!=typeof g&&b.JSON&&JSON.hasOwnProperty("stringify")?(m[s]||(m[s]={}),m[s][r]=g,l?b[p][f]=JSON.stringify(m):(k=new Date,k.setTime(k.getTime()+31536e6),c.cookie=f+"="+JSON.stringify(m).replace(/\"/g,'"')+"; expires="+k.toGMTString()+"; path=/"),void 0):m&&m[s]?m[s][r]:""}}(a,window,document),function(a){"use strict";var b=a.tablesorter||{};b.themes={bootstrap:{table:"table table-bordered table-striped",caption:"caption",header:"bootstrap-header",sortNone:"",sortAsc:"",sortDesc:"",active:"",hover:"",icons:"",iconSortNone:"bootstrap-icon-unsorted",iconSortAsc:"icon-chevron-up glyphicon glyphicon-chevron-up",iconSortDesc:"icon-chevron-down glyphicon glyphicon-chevron-down",filterRow:"",footerRow:"",footerCells:"",even:"",odd:""},jui:{table:"ui-widget ui-widget-content ui-corner-all",caption:"ui-widget-content",header:"ui-widget-header ui-corner-all ui-state-default",sortNone:"",sortAsc:"",sortDesc:"",active:"ui-state-active",hover:"ui-state-hover",icons:"ui-icon",iconSortNone:"ui-icon-carat-2-n-s ui-icon-caret-2-n-s",iconSortAsc:"ui-icon-carat-1-n ui-icon-caret-1-n",iconSortDesc:"ui-icon-carat-1-s ui-icon-caret-1-s",filterRow:"",footerRow:"",footerCells:"",even:"ui-widget-content",odd:"ui-state-default"}},a.extend(b.css,{wrapper:"tablesorter-wrapper"}),b.addWidget({id:"uitheme",priority:10,format:function(c,d,e){var f,g,h,i,j,k,l,m,n,o,p,q,r,s=b.themes,t=d.$table.add(a(d.namespace+"_extra_table")),u=d.$headers.add(a(d.namespace+"_extra_headers")),v=d.theme||"jui",w=s[v]||{},x=a.trim([w.sortNone,w.sortDesc,w.sortAsc,w.active].join(" ")),y=a.trim([w.iconSortNone,w.iconSortDesc,w.iconSortAsc].join(" "));for(d.debug&&(j=new Date),t.hasClass("tablesorter-"+v)&&d.theme===d.appliedTheme&&e.uitheme_applied||(e.uitheme_applied=!0,o=s[d.appliedTheme]||{},r=!a.isEmptyObject(o),p=r?[o.sortNone,o.sortDesc,o.sortAsc,o.active].join(" "):"",q=r?[o.iconSortNone,o.iconSortDesc,o.iconSortAsc].join(" "):"",r&&(e.zebra[0]=a.trim(" "+e.zebra[0].replace(" "+o.even,"")),e.zebra[1]=a.trim(" "+e.zebra[1].replace(" "+o.odd,"")),d.$tbodies.children().removeClass([o.even,o.odd].join(" "))),w.even&&(e.zebra[0]+=" "+w.even),w.odd&&(e.zebra[1]+=" "+w.odd),t.children("caption").removeClass(o.caption||"").addClass(w.caption),m=t.removeClass((d.appliedTheme?"tablesorter-"+(d.appliedTheme||""):"")+" "+(o.table||"")).addClass("tablesorter-"+v+" "+(w.table||"")).children("tfoot"),d.appliedTheme=d.theme,m.length&&m.children("tr").removeClass(o.footerRow||"").addClass(w.footerRow).children("th, td").removeClass(o.footerCells||"").addClass(w.footerCells),u.removeClass((r?[o.header,o.hover,p].join(" "):"")||"").addClass(w.header).not(".sorter-false").unbind("mouseenter.tsuitheme mouseleave.tsuitheme").bind("mouseenter.tsuitheme mouseleave.tsuitheme",function(b){a(this)["mouseenter"===b.type?"addClass":"removeClass"](w.hover||"")}),u.each(function(){var c=a(this);c.find("."+b.css.wrapper).length||c.wrapInner('<div class="'+b.css.wrapper+'" style="position:relative;height:100%;width:100%"></div>')}),d.cssIcon&&u.find("."+b.css.icon).removeClass(r?[o.icons,q].join(" "):"").addClass(w.icons||""),b.hasWidget(d.table,"filter")&&(g=function(){t.children("thead").children("."+b.css.filterRow).removeClass(r?o.filterRow||"":"").addClass(w.filterRow||"")},e.filter_initialized?g():t.one("filterInit",function(){g()}))),f=0;f<d.columns;f++)k=d.$headers.add(a(d.namespace+"_extra_headers")).not(".sorter-false").filter('[data-column="'+f+'"]'),l=b.css.icon?k.find("."+b.css.icon):a(),n=u.not(".sorter-false").filter('[data-column="'+f+'"]:last'),n.length&&(k.removeClass(x),l.removeClass(y),n[0].sortDisabled?l.removeClass(w.icons||""):(h=w.sortNone,i=w.iconSortNone,n.hasClass(b.css.sortAsc)?(h=[w.sortAsc,w.active].join(" "),i=w.iconSortAsc):n.hasClass(b.css.sortDesc)&&(h=[w.sortDesc,w.active].join(" "),i=w.iconSortDesc),k.addClass(h),l.addClass(i||"")));d.debug&&console.log("Applying "+v+" theme"+b.benchmark(j))},remove:function(a,c,d,e){if(d.uitheme_applied){var f=c.$table,g=c.appliedTheme||"jui",h=b.themes[g]||b.themes.jui,i=f.children("thead").children(),j=h.sortNone+" "+h.sortDesc+" "+h.sortAsc,k=h.iconSortNone+" "+h.iconSortDesc+" "+h.iconSortAsc;f.removeClass("tablesorter-"+g+" "+h.table),d.uitheme_applied=!1,e||(f.find(b.css.header).removeClass(h.header),i.unbind("mouseenter.tsuitheme mouseleave.tsuitheme").removeClass(h.hover+" "+j+" "+h.active).filter("."+b.css.filterRow).removeClass(h.filterRow),i.find("."+b.css.icon).removeClass(h.icons+" "+k))}}})}(a),function(a){"use strict";var b=a.tablesorter||{};b.addWidget({id:"columns",priority:30,options:{columns:["primary","secondary","tertiary"]},format:function(c,d,e){var f,g,h,i,j,k,l,m,n=d.$table,o=d.$tbodies,p=d.sortList,q=p.length,r=e&&e.columns||["primary","secondary","tertiary"],s=r.length-1;for(l=r.join(" "),g=0;g<o.length;g++)f=b.processTbody(c,o.eq(g),!0),h=f.children("tr"),h.each(function(){if(j=a(this),"none"!==this.style.display&&(k=j.children().removeClass(l),p&&p[0]&&(k.eq(p[0][0]).addClass(r[0]),q>1)))for(m=1;m<q;m++)k.eq(p[m][0]).addClass(r[m]||r[s])}),b.processTbody(c,f,!1);if(i=e.columns_thead!==!1?["thead tr"]:[],e.columns_tfoot!==!1&&i.push("tfoot tr"),i.length&&(h=n.find(i.join(",")).children().removeClass(l),q))for(m=0;m<q;m++)h.filter('[data-column="'+p[m][0]+'"]').addClass(r[m]||r[s])},remove:function(c,d,e){var f,g,h=d.$tbodies,i=(e.columns||["primary","secondary","tertiary"]).join(" ");for(d.$headers.removeClass(i),d.$table.children("tfoot").children("tr").children("th, td").removeClass(i),f=0;f<h.length;f++)g=b.processTbody(c,h.eq(f),!0),g.children("tr").each(function(){a(this).children().removeClass(i)}),b.processTbody(c,g,!1)}})}(a),function(a){"use strict";var b,c,d=a.tablesorter||{},e=d.css,f=d.keyCodes;a.extend(e,{filterRow:"tablesorter-filter-row",filter:"tablesorter-filter",filterDisabled:"disabled",filterRowHide:"hideme"}),a.extend(f,{backSpace:8,escape:27,space:32,left:37,down:40}),d.addWidget({id:"filter",priority:50,options:{filter_cellFilter:"",filter_childRows:!1,filter_childByColumn:!1,filter_childWithSibs:!0,filter_columnAnyMatch:!0,filter_columnFilters:!0,filter_cssFilter:"",filter_defaultAttrib:"data-value",filter_defaultFilter:{},filter_excludeFilter:{},filter_external:"",filter_filteredRow:"filtered",filter_formatter:null,filter_functions:null,filter_hideEmpty:!0,filter_hideFilters:!1,filter_ignoreCase:!0,filter_liveSearch:!0,filter_matchType:{input:"exact",select:"exact"},filter_onlyAvail:"filter-onlyAvail",filter_placeholder:{search:"",select:""},filter_reset:null,filter_resetOnEsc:!0,filter_saveFilters:!1,filter_searchDelay:300,filter_searchFiltered:!0,filter_selectSource:null,filter_selectSourceSeparator:"|",filter_serversideFiltering:!1,filter_startsWith:!1,filter_useParsedData:!1},format:function(a,c,d){c.$table.hasClass("hasFilters")||b.init(a,c,d)},remove:function(b,c,f,g){var h,i,j=c.$table,k=c.$tbodies,l="addRows updateCell update updateRows updateComplete appendCache filterReset filterEnd search ".split(" ").join(c.namespace+"filter ");if(j.removeClass("hasFilters").unbind(l.replace(d.regex.spaces," ")).find("."+e.filterRow).remove(),f.filter_initialized=!1,!g){for(h=0;h<k.length;h++)i=d.processTbody(b,k.eq(h),!0),i.children().removeClass(f.filter_filteredRow).show(),d.processTbody(b,i,!1);f.filter_reset&&a(document).undelegate(f.filter_reset,"click"+c.namespace+"filter")}}}),b=d.filter={regex:{regex:/^\/((?:\\\/|[^\/])+)\/([migyu]{0,5})?$/,child:/tablesorter-childRow/,filtered:/filtered/,type:/undefined|number/,exact:/(^[\"\'=]+)|([\"\'=]+$)/g,operators:/[<>=]/g,query:"(q|query)",wild01:/\?/g,wild0More:/\*/g,quote:/\"/g,isNeg1:/(>=?\s*-\d)/,isNeg2:/(<=?\s*\d)/},types:{or:function(d,e,f){if((c.orTest.test(e.iFilter)||c.orSplit.test(e.filter))&&!c.regex.test(e.filter)){var g,h,i,j,k=a.extend({},e),l=e.filter.split(c.orSplit),m=e.iFilter.split(c.orSplit),n=l.length;for(g=0;g<n;g++){k.nestedFilters=!0,k.filter=""+(b.parseFilter(d,l[g],e)||""),k.iFilter=""+(b.parseFilter(d,m[g],e)||""),i="("+(b.parseFilter(d,k.filter,e)||"")+")";try{if(j=new RegExp(e.isMatch?i:"^"+i+"$",d.widgetOptions.filter_ignoreCase?"i":""),h=j.test(k.exact)||b.processTypes(d,k,f))return h}catch(a){return null}}return h||!1}return null},and:function(d,e,f){if(c.andTest.test(e.filter)){var g,h,i,j,k,l=a.extend({},e),m=e.filter.split(c.andSplit),n=e.iFilter.split(c.andSplit),o=m.length;for(g=0;g<o;g++){l.nestedFilters=!0,l.filter=""+(b.parseFilter(d,m[g],e)||""),l.iFilter=""+(b.parseFilter(d,n[g],e)||""),j=("("+(b.parseFilter(d,l.filter,e)||"")+")").replace(c.wild01,"\\S{1}").replace(c.wild0More,"\\S*");try{k=new RegExp(e.isMatch?j:"^"+j+"$",d.widgetOptions.filter_ignoreCase?"i":""),i=k.test(l.exact)||b.processTypes(d,l,f),h=0===g?i:h&&i}catch(a){return null}}return h||!1}return null},regex:function(a,b){if(c.regex.test(b.filter)){var d,e=b.filter_regexCache[b.index]||c.regex.exec(b.filter),f=e instanceof RegExp;try{f||(b.filter_regexCache[b.index]=e=new RegExp(e[1],e[2])),d=e.test(b.exact)}catch(a){d=!1}return d}return null},operators:function(e,f){if(c.operTest.test(f.iFilter)&&""!==f.iExact){var g,h,i,j=e.table,k=f.parsed[f.index],l=d.formatFloat(f.iFilter.replace(c.operators,""),j),m=e.parsers[f.index]||{},n=l;return(k||"numeric"===m.type)&&(i=a.trim(""+f.iFilter.replace(c.operators,"")),h=b.parseFilter(e,i,f,!0),l="number"!=typeof h||""===h||isNaN(h)?l:h),!k&&"numeric"!==m.type||isNaN(l)||"undefined"==typeof f.cache?(i=isNaN(f.iExact)?f.iExact.replace(d.regex.nondigit,""):f.iExact,g=d.formatFloat(i,j)):g=f.cache,c.gtTest.test(f.iFilter)?h=c.gteTest.test(f.iFilter)?g>=l:g>l:c.ltTest.test(f.iFilter)&&(h=c.lteTest.test(f.iFilter)?g<=l:g<l),h||""!==n||(h=!0),h}return null},notMatch:function(d,e){if(c.notTest.test(e.iFilter)){var f,g=e.iFilter.replace("!",""),h=b.parseFilter(d,g,e)||"";return c.exact.test(h)?(h=h.replace(c.exact,""),""===h||a.trim(h)!==e.iExact):(f=e.iExact.search(a.trim(h)),""===h||(e.anyMatch?f<0:!(d.widgetOptions.filter_startsWith?0===f:f>=0)))}return null},exact:function(d,e){if(c.exact.test(e.iFilter)){var f=e.iFilter.replace(c.exact,""),g=b.parseFilter(d,f,e)||"";return e.anyMatch?a.inArray(g,e.rowArray)>=0:g==e.iExact}return null},range:function(a,e){if(c.toTest.test(e.iFilter)){var f,g,h,i,j=a.table,k=e.index,l=e.parsed[k],m=e.iFilter.split(c.toSplit);return g=m[0].replace(d.regex.nondigit,"")||"",h=d.formatFloat(b.parseFilter(a,g,e),j),g=m[1].replace(d.regex.nondigit,"")||"",i=d.formatFloat(b.parseFilter(a,g,e),j),(l||"numeric"===a.parsers[k].type)&&(f=a.parsers[k].format(""+m[0],j,a.$headers.eq(k),k),h=""===f||isNaN(f)?h:f,f=a.parsers[k].format(""+m[1],j,a.$headers.eq(k),k),i=""===f||isNaN(f)?i:f),!l&&"numeric"!==a.parsers[k].type||isNaN(h)||isNaN(i)?(g=isNaN(e.iExact)?e.iExact.replace(d.regex.nondigit,""):e.iExact,f=d.formatFloat(g,j)):f=e.cache,h>i&&(g=h,h=i,i=g),f>=h&&f<=i||""===h||""===i}return null},wild:function(a,d){if(c.wildOrTest.test(d.iFilter)){var e=""+(b.parseFilter(a,d.iFilter,d)||"");!c.wildTest.test(e)&&d.nestedFilters&&(e=d.isMatch?e:"^("+e+")$");try{return new RegExp(e.replace(c.wild01,"\\S{1}").replace(c.wild0More,"\\S*"),a.widgetOptions.filter_ignoreCase?"i":"").test(d.exact)}catch(a){return null}}return null},fuzzy:function(a,d){if(c.fuzzyTest.test(d.iFilter)){var e,f=0,g=d.iExact.length,h=d.iFilter.slice(1),i=b.parseFilter(a,h,d)||"";for(e=0;e<g;e++)d.iExact[e]===i[f]&&(f+=1);return f===i.length}return null}},init:function(f){d.language=a.extend(!0,{},{to:"to",or:"or",and:"and"},d.language);var g,h,i,j,k,l,m,n,o=f.config,p=o.widgetOptions;if(o.$table.addClass("hasFilters"),o.lastSearch=[],p.filter_searchTimer=null,p.filter_initTimer=null,p.filter_formatterCount=0,p.filter_formatterInit=[],p.filter_anyColumnSelector='[data-column="all"],[data-column="any"]',p.filter_multipleColumnSelector='[data-column*="-"],[data-column*=","]',l="\\{"+c.query+"\\}",a.extend(c,{child:new RegExp(o.cssChildRow),filtered:new RegExp(p.filter_filteredRow),alreadyFiltered:new RegExp("(\\s+("+d.language.or+"|-|"+d.language.to+")\\s+)","i"),toTest:new RegExp("\\s+(-|"+d.language.to+")\\s+","i"),toSplit:new RegExp("(?:\\s+(?:-|"+d.language.to+")\\s+)","gi"),andTest:new RegExp("\\s+("+d.language.and+"|&&)\\s+","i"),andSplit:new RegExp("(?:\\s+(?:"+d.language.and+"|&&)\\s+)","gi"),orTest:new RegExp("(\\||\\s+"+d.language.or+"\\s+)","i"),orSplit:new RegExp("(?:\\s+(?:"+d.language.or+")\\s+|\\|)","gi"),iQuery:new RegExp(l,"i"),igQuery:new RegExp(l,"ig"),operTest:/^[<>]=?/,gtTest:/>/,gteTest:/>=/,ltTest:/</,lteTest:/<=/,notTest:/^\!/,wildOrTest:/[\?\*\|]/,wildTest:/\?\*/,fuzzyTest:/^~/,exactTest:/[=\"\|!]/}),l=o.$headers.filter(".filter-false, .parser-false").length,p.filter_columnFilters!==!1&&l!==o.$headers.length&&b.buildRow(f,o,p),i="addRows updateCell update updateRows updateComplete appendCache filterReset "+"filterResetSaved filterEnd search ".split(" ").join(o.namespace+"filter "),o.$table.bind(i,function(c,g){return l=p.filter_hideEmpty&&a.isEmptyObject(o.cache)&&!(o.delayInit&&"appendCache"===c.type),o.$table.find("."+e.filterRow).toggleClass(p.filter_filteredRow,l),/(search|filter)/.test(c.type)||(c.stopPropagation(),b.buildDefault(f,!0)),"filterReset"===c.type?(o.$table.find("."+e.filter).add(p.filter_$externalFilters).val(""),b.searching(f,[])):"filterResetSaved"===c.type?d.storage(f,"tablesorter-filters",""):"filterEnd"===c.type?b.buildDefault(f,!0):(g="search"===c.type?g:"updateComplete"===c.type?o.$table.data("lastSearch"):"",/(update|add)/.test(c.type)&&"updateComplete"!==c.type&&(o.lastCombinedFilter=null,o.lastSearch=[],setTimeout(function(){o.$table.triggerHandler("filterFomatterUpdate")},100)),b.searching(f,g,!0)),!1}),p.filter_reset&&(p.filter_reset instanceof a?p.filter_reset.click(function(){o.$table.triggerHandler("filterReset")}):a(p.filter_reset).length&&a(document).undelegate(p.filter_reset,"click"+o.namespace+"filter").delegate(p.filter_reset,"click"+o.namespace+"filter",function(){o.$table.triggerHandler("filterReset")})),p.filter_functions)for(k=0;k<o.columns;k++)if(m=d.getColumnData(f,p.filter_functions,k))if(j=o.$headerIndexed[k].removeClass("filter-select"),n=!(j.hasClass("filter-false")||j.hasClass("parser-false")),g="",m===!0&&n)b.buildSelect(f,k);else if("object"==typeof m&&n){for(h in m)"string"==typeof h&&(g+=""===g?'<option value="">'+(j.data("placeholder")||j.attr("data-placeholder")||p.filter_placeholder.select||"")+"</option>":"",l=h,i=h,h.indexOf(p.filter_selectSourceSeparator)>=0&&(l=h.split(p.filter_selectSourceSeparator),i=l[1],l=l[0]),g+="<option "+(i===l?"":'data-function-name="'+h+'" ')+'value="'+l+'">'+i+"</option>");o.$table.find("thead").find("select."+e.filter+'[data-column="'+k+'"]').append(g),i=p.filter_selectSource,m="function"==typeof i||d.getColumnData(f,i,k),m&&b.buildSelect(o.table,k,"",!0,j.hasClass(p.filter_onlyAvail))}b.buildDefault(f,!0),b.bindSearch(f,o.$table.find("."+e.filter),!0),p.filter_external&&b.bindSearch(f,p.filter_external),p.filter_hideFilters&&b.hideFilters(o),o.showProcessing&&(i="filterStart filterEnd ".split(" ").join(o.namespace+"filter "),o.$table.unbind(i.replace(d.regex.spaces," ")).bind(i,function(b,c){j=c?o.$table.find("."+e.header).filter("[data-column]").filter(function(){return""!==c[a(this).data("column")]}):"",d.isProcessing(f,"filterStart"===b.type,c?j:"")})),o.filteredRows=o.totalRows,i="tablesorter-initialized pagerBeforeInitialized ".split(" ").join(o.namespace+"filter "),o.$table.unbind(i.replace(d.regex.spaces," ")).bind(i,function(){b.completeInit(this)}),o.pager&&o.pager.initialized&&!p.filter_initialized?(o.$table.triggerHandler("filterFomatterUpdate"),setTimeout(function(){b.filterInitComplete(o)},100)):p.filter_initialized||b.completeInit(f)},completeInit:function(a){var c=a.config,e=c.widgetOptions,f=b.setDefaults(a,c,e)||[];f.length&&(c.delayInit&&""===f.join("")||d.setFilters(a,f,!0)),c.$table.triggerHandler("filterFomatterUpdate"),setTimeout(function(){e.filter_initialized||b.filterInitComplete(c)},100)},formatterUpdated:function(a,b){var c=a&&a.closest("table")[0].config.widgetOptions;c&&!c.filter_initialized&&(c.filter_formatterInit[b]=1)},filterInitComplete:function(c){var d,e,f=c.widgetOptions,g=0,h=function(){f.filter_initialized=!0,c.lastSearch=c.$table.data("lastSearch"),c.$table.triggerHandler("filterInit",c),b.findRows(c.table,c.lastSearch||[])};if(a.isEmptyObject(f.filter_formatter))h();else{for(e=f.filter_formatterInit.length,d=0;d<e;d++)1===f.filter_formatterInit[d]&&g++;clearTimeout(f.filter_initTimer),f.filter_initialized||g!==f.filter_formatterCount?f.filter_initialized||(f.filter_initTimer=setTimeout(function(){h()},500)):h()}},processFilters:function(a,b){var c,d=[],e=b?encodeURIComponent:decodeURIComponent,f=a.length;for(c=0;c<f;c++)a[c]&&(d[c]=e(a[c]));return d},setDefaults:function(c,e,f){var g,h,i,j,k,l=d.getFilters(c)||[];if(f.filter_saveFilters&&d.storage&&(h=d.storage(c,"tablesorter-filters")||[],g=a.isArray(h),g&&""===h.join("")||!g||(l=b.processFilters(h))),""===l.join(""))for(k=e.$headers.add(f.filter_$externalFilters).filter("["+f.filter_defaultAttrib+"]"),i=0;i<=e.columns;i++)j=i===e.columns?"all":i,l[i]=k.filter('[data-column="'+j+'"]').attr(f.filter_defaultAttrib)||l[i]||"";return e.$table.data("lastSearch",l),l},parseFilter:function(a,b,c,d){return d||c.parsed[c.index]?a.parsers[c.index].format(b,a.table,[],c.index):b},buildRow:function(c,f,g){var h,i,j,k,l,m,n,o,p,q=g.filter_cellFilter,r=f.columns,s=a.isArray(q),t='<tr role="row" class="'+e.filterRow+" "+f.cssIgnoreRow+'">';for(j=0;j<r;j++)f.$headerIndexed[j].length&&(p=f.$headerIndexed[j]&&f.$headerIndexed[j][0].colSpan||0,t+=p>1?'<td data-column="'+j+"-"+(j+p-1)+'" colspan="'+p+'"':'<td data-column="'+j+'"',t+=s?q[j]?' class="'+q[j]+'"':"":""!==q?' class="'+q+'"':"",t+="></td>");for(f.$filters=a(t+="</tr>").appendTo(f.$table.children("thead").eq(0)).children("td"),j=0;j<r;j++)m=!1,k=f.$headerIndexed[j],k&&k.length&&(h=b.getColumnElm(f,f.$filters,j),o=d.getColumnData(c,g.filter_functions,j),l=g.filter_functions&&o&&"function"!=typeof o||k.hasClass("filter-select"),i=d.getColumnData(c,f.headers,j),m="false"===d.getData(k[0],i,"filter")||"false"===d.getData(k[0],i,"parser"),l?t=a("<select>").appendTo(h):(o=d.getColumnData(c,g.filter_formatter,j),o?(g.filter_formatterCount++,t=o(h,j),t&&0===t.length&&(t=h.children("input")),t&&(0===t.parent().length||t.parent().length&&t.parent()[0]!==h[0])&&h.append(t)):t=a('<input type="search">').appendTo(h),t&&(p=k.data("placeholder")||k.attr("data-placeholder")||g.filter_placeholder.search||"",t.attr("placeholder",p))),t&&(n=(a.isArray(g.filter_cssFilter)?"undefined"!=typeof g.filter_cssFilter[j]?g.filter_cssFilter[j]||"":"":g.filter_cssFilter)||"",t.addClass(e.filter+" "+n).attr("data-column",h.attr("data-column")),m&&(t.attr("placeholder","").addClass(e.filterDisabled)[0].disabled=!0)))},bindSearch:function(c,e,g){if(c=a(c)[0],e=a(e),e.length){var h,i=c.config,j=i.widgetOptions,k=i.namespace+"filter",l=j.filter_$externalFilters;g!==!0&&(h=j.filter_anyColumnSelector+","+j.filter_multipleColumnSelector,j.filter_$anyMatch=e.filter(h),l&&l.length?j.filter_$externalFilters=j.filter_$externalFilters.add(e):j.filter_$externalFilters=e,d.setFilters(c,i.$table.data("lastSearch")||[],g===!1)),h="keypress keyup keydown search change input ".split(" ").join(k+" "),e.attr("data-lastSearchTime",(new Date).getTime()).unbind(h.replace(d.regex.spaces," ")).bind("keydown"+k,function(a){if(a.which===f.escape&&!c.config.widgetOptions.filter_resetOnEsc)return!1}).bind("keyup"+k,function(e){j=c.config.widgetOptions;var g=parseInt(a(this).attr("data-column"),10),h="boolean"==typeof j.filter_liveSearch?j.filter_liveSearch:d.getColumnData(c,j.filter_liveSearch,g);if("undefined"==typeof h&&(h=j.filter_liveSearch.fallback||!1),a(this).attr("data-lastSearchTime",(new Date).getTime()),e.which===f.escape)this.value=j.filter_resetOnEsc?"":i.lastSearch[g];else{if(h===!1)return;if(""!==this.value&&("number"==typeof h&&this.value.length<h||e.which!==f.enter&&e.which!==f.backSpace&&(e.which<f.space||e.which>=f.left&&e.which<=f.down)))return}b.searching(c,!0,!0,g)}).bind("search change keypress input ".split(" ").join(k+" "),function(e){var g=parseInt(a(this).attr("data-column"),10),h="boolean"==typeof j.filter_liveSearch?j.filter_liveSearch:d.getColumnData(c,j.filter_liveSearch,g);!c.config.widgetOptions.filter_initialized||e.which!==f.enter&&"search"!==e.type&&("change"!==e.type&&"input"!==e.type||h!==!0||this.value===i.lastSearch[g])||(e.preventDefault(),a(this).attr("data-lastSearchTime",(new Date).getTime()),b.searching(c,"keypress"!==e.type,!0,g))})}},searching:function(a,c,e,f){var g,h=a.config.widgetOptions;"undefined"==typeof f?g=!1:(g="boolean"==typeof h.filter_liveSearch?h.filter_liveSearch:d.getColumnData(a,h.filter_liveSearch,f),"undefined"==typeof g&&(g=h.filter_liveSearch.fallback||!1)),clearTimeout(h.filter_searchTimer),"undefined"==typeof c||c===!0?h.filter_searchTimer=setTimeout(function(){b.checkFilters(a,c,e)},g?h.filter_searchDelay:10):b.checkFilters(a,c,e)},checkFilters:function(c,f,g){var h=c.config,i=h.widgetOptions,j=a.isArray(f),k=j?f:d.getFilters(c,!0),l=(k||[]).join("");return a.isEmptyObject(h.cache)?void(h.delayInit&&(!h.pager||h.pager&&h.pager.initialized)&&d.updateCache(h,function(){b.checkFilters(c,!1,g)})):(j&&(d.setFilters(c,k,!1,g!==!0),i.filter_initialized||(h.lastCombinedFilter="")),i.filter_hideFilters&&h.$table.find("."+e.filterRow).triggerHandler(b.hideFiltersCheck(h)?"mouseleave":"mouseenter"),h.lastCombinedFilter!==l||f===!1?(f===!1&&(h.lastCombinedFilter=null,h.lastSearch=[]),k=k||[],k=Array.prototype.map?k.map(String):k.join("�").split("�"),i.filter_initialized&&h.$table.triggerHandler("filterStart",[k]),h.showProcessing?void setTimeout(function(){return b.findRows(c,k,l),!1},30):(b.findRows(c,k,l),!1)):void 0)},hideFiltersCheck:function(a){if("function"==typeof a.widgetOptions.filter_hideFilters){var b=a.widgetOptions.filter_hideFilters(a);if("boolean"==typeof b)return b}return""===d.getFilters(a.$table).join("")},hideFilters:function(c,d){var f;(d||c.$table).find("."+e.filterRow).addClass(e.filterRowHide).bind("mouseenter mouseleave",function(d){var g=d,h=a(this);clearTimeout(f),f=setTimeout(function(){/enter|over/.test(g.type)?h.removeClass(e.filterRowHide):a(document.activeElement).closest("tr")[0]!==h[0]&&h.toggleClass(e.filterRowHide,b.hideFiltersCheck(c))},200)}).find("input, select").bind("focus blur",function(d){var g=d,h=a(this).closest("tr");clearTimeout(f),f=setTimeout(function(){clearTimeout(f),h.toggleClass(e.filterRowHide,b.hideFiltersCheck(c)&&"focus"!==g.type)},200)})},defaultFilter:function(b,d){if(""===b)return b;var e=c.iQuery,f=d.match(c.igQuery).length,g=f>1?a.trim(b).split(/\s/):[a.trim(b)],h=g.length-1,i=0,j=d;for(h<1&&f>1&&(g[1]=g[0]);e.test(j);)j=j.replace(e,g[i++]||""),e.test(j)&&i<h&&""!==(g[i]||"")&&(j=d.replace(e,j));return j},getLatestSearch:function(b){return b?b.sort(function(b,c){return a(c).attr("data-lastSearchTime")-a(b).attr("data-lastSearchTime")}):b||a()},findRange:function(a,b,c){var d,e,f,g,h,i,j,k,l,m=[];if(/^[0-9]+$/.test(b))return[parseInt(b,10)];if(!c&&/-/.test(b))for(e=b.match(/(\d+)\s*-\s*(\d+)/g),l=e?e.length:0,k=0;k<l;k++){for(f=e[k].split(/\s*-\s*/),g=parseInt(f[0],10)||0,h=parseInt(f[1],10)||a.columns-1,g>h&&(d=g,g=h,h=d),h>=a.columns&&(h=a.columns-1);g<=h;g++)m[m.length]=g;b=b.replace(e[k],"")}if(!c&&/,/.test(b))for(i=b.split(/\s*,\s*/),l=i.length,j=0;j<l;j++)""!==i[j]&&(k=parseInt(i[j],10),k<a.columns&&(m[m.length]=k));if(!m.length)for(k=0;k<a.columns;k++)m[m.length]=k;return m},getColumnElm:function(c,d,e){return d.filter(function(){var d=b.findRange(c,a(this).attr("data-column"));return a.inArray(e,d)>-1})},multipleColumns:function(c,d){var e=c.widgetOptions,f=e.filter_initialized||!d.filter(e.filter_anyColumnSelector).length,g=a.trim(b.getLatestSearch(d).attr("data-column")||"");return b.findRange(c,g,!f)},processTypes:function(c,d,e){var f,g=null,h=null;for(f in b.types)a.inArray(f,e.excludeMatch)<0&&null===h&&(h=b.types[f](c,d,e),null!==h&&(g=h));return g},matchType:function(a,b){var c,d=a.widgetOptions,f=a.$headerIndexed[b];return f.hasClass("filter-exact")?c=!1:f.hasClass("filter-match")?c=!0:(d.filter_columnFilters?f=a.$filters.find("."+e.filter).add(d.filter_$externalFilters).filter('[data-column="'+b+'"]'):d.filter_$externalFilters&&(f=d.filter_$externalFilters.filter('[data-column="'+b+'"]')),c=!!f.length&&"match"===a.widgetOptions.filter_matchType[(f[0].nodeName||"").toLowerCase()]),c},processRow:function(e,f,g){var h,i,j,k,l,m=e.widgetOptions,n=!0,o=m.filter_$anyMatch&&m.filter_$anyMatch.length,p=m.filter_$anyMatch&&m.filter_$anyMatch.length?b.multipleColumns(e,m.filter_$anyMatch):[];if(f.$cells=f.$row.children(),f.anyMatchFlag&&p.length>1||f.anyMatchFilter&&!o){if(f.anyMatch=!0,f.isMatch=!0,f.rowArray=f.$cells.map(function(b){if(a.inArray(b,p)>-1||f.anyMatchFilter&&!o)return f.parsed[b]?l=f.cacheArray[b]:(l=f.rawArray[b],l=a.trim(m.filter_ignoreCase?l.toLowerCase():l),e.sortLocaleCompare&&(l=d.replaceAccents(l))),l}).get(),f.filter=f.anyMatchFilter,f.iFilter=f.iAnyMatchFilter,f.exact=f.rowArray.join(" "),f.iExact=m.filter_ignoreCase?f.exact.toLowerCase():f.exact,f.cache=f.cacheArray.slice(0,-1).join(" "),g.excludeMatch=g.noAnyMatch,i=b.processTypes(e,f,g),null!==i)n=i;else if(m.filter_startsWith)for(n=!1,p=Math.min(e.columns,f.rowArray.length);!n&&p>0;)p--,n=n||0===f.rowArray[p].indexOf(f.iFilter);else n=(f.iExact+f.childRowText).indexOf(f.iFilter)>=0;if(f.anyMatch=!1,f.filters.join("")===f.filter)return n}for(p=0;p<e.columns;p++)f.filter=f.filters[p],f.index=p,g.excludeMatch=g.excludeFilter[p],f.filter&&(f.cache=f.cacheArray[p],h=f.parsed[p]?f.cache:f.rawArray[p]||"",f.exact=e.sortLocaleCompare?d.replaceAccents(h):h,f.iExact=!c.type.test(typeof f.exact)&&m.filter_ignoreCase?f.exact.toLowerCase():f.exact,f.isMatch=b.matchType(e,p),h=n,k=m.filter_columnFilters?e.$filters.add(m.filter_$externalFilters).filter('[data-column="'+p+'"]').find("select option:selected").attr("data-function-name")||"":"",e.sortLocaleCompare&&(f.filter=d.replaceAccents(f.filter)),m.filter_defaultFilter&&c.iQuery.test(g.defaultColFilter[p])&&(f.filter=b.defaultFilter(f.filter,g.defaultColFilter[p])),f.iFilter=m.filter_ignoreCase?(f.filter||"").toLowerCase():f.filter,j=g.functions[p],i=null,j&&(j===!0?i=f.isMatch?(""+f.iExact).search(f.iFilter)>=0:f.filter===f.exact:"function"==typeof j?i=j(f.exact,f.cache,f.filter,p,f.$row,e,f):"function"==typeof j[k||f.filter]&&(l=k||f.filter,i=j[l](f.exact,f.cache,f.filter,p,f.$row,e,f))),null===i?(i=b.processTypes(e,f,g),null!==i?h=i:(l=(f.iExact+f.childRowText).indexOf(b.parseFilter(e,f.iFilter,f)),h=!m.filter_startsWith&&l>=0||m.filter_startsWith&&0===l)):h=i,n=!!h&&n);return n},findRows:function(e,f,g){if(e.config.lastCombinedFilter!==g&&e.config.widgetOptions.filter_initialized){var h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,A,B,C,D,E,F=a.extend([],f),G=e.config,H=G.widgetOptions,I={anyMatch:!1,filters:f,filter_regexCache:[]},J={noAnyMatch:["range","operators"],functions:[],excludeFilter:[],defaultColFilter:[],defaultAnyFilter:d.getColumnData(e,H.filter_defaultFilter,G.columns,!0)||""};for(I.parsed=[],p=0;p<G.columns;p++)I.parsed[p]=H.filter_useParsedData||G.parsers&&G.parsers[p]&&G.parsers[p].parsed||d.getData&&"parsed"===d.getData(G.$headerIndexed[p],d.getColumnData(e,G.headers,p),"filter")||G.$headerIndexed[p].hasClass("filter-parsed"),J.functions[p]=d.getColumnData(e,H.filter_functions,p)||G.$headerIndexed[p].hasClass("filter-select"),J.defaultColFilter[p]=d.getColumnData(e,H.filter_defaultFilter,p)||"",J.excludeFilter[p]=(d.getColumnData(e,H.filter_excludeFilter,p,!0)||"").split(/\s+/);for(G.debug&&(console.log("Filter: Starting filter widget search",f),v=new Date),G.filteredRows=0,G.totalRows=0,g=(F||[]).join(""),n=0;n<G.$tbodies.length;n++){if(o=d.processTbody(e,G.$tbodies.eq(n),!0),p=G.columns,i=G.cache[n].normalized,k=a(a.map(i,function(a){return a[p].$row.get()})),""===g||H.filter_serversideFiltering)k.removeClass(H.filter_filteredRow).not("."+G.cssChildRow).css("display","");else{if(k=k.not("."+G.cssChildRow),h=k.length,(H.filter_$anyMatch&&H.filter_$anyMatch.length||"undefined"!=typeof f[G.columns])&&(I.anyMatchFlag=!0,I.anyMatchFilter=""+(f[G.columns]||H.filter_$anyMatch&&b.getLatestSearch(H.filter_$anyMatch).val()||""),H.filter_columnAnyMatch)){for(A=I.anyMatchFilter.split(c.andSplit),B=!1,x=0;x<A.length;x++)C=A[x].split(":"),C.length>1&&(isNaN(C[0])?a.each(G.headerContent,function(a,b){b.toLowerCase().indexOf(C[0])>-1&&(D=a,f[D]=C[1])}):D=parseInt(C[0],10)-1,D>=0&&D<G.columns&&(f[D]=C[1],A.splice(x,1),x--,B=!0));B&&(I.anyMatchFilter=A.join(" && "))}if(z=H.filter_searchFiltered,s=G.lastSearch||G.$table.data("lastSearch")||[],z)for(x=0;x<p+1;x++)w=f[x]||"",z||(x=p),z=z&&s.length&&0===w.indexOf(s[x]||"")&&!c.alreadyFiltered.test(w)&&!c.exactTest.test(w)&&!(c.isNeg1.test(w)||c.isNeg2.test(w))&&!(""!==w&&G.$filters&&G.$filters.filter('[data-column="'+x+'"]').find("select").length&&!b.matchType(G,x));for(y=k.not("."+H.filter_filteredRow).length,z&&0===y&&(z=!1),G.debug&&console.log("Filter: Searching through "+(z&&y<h?y:"all")+" rows"),I.anyMatchFlag&&(G.sortLocaleCompare&&(I.anyMatchFilter=d.replaceAccents(I.anyMatchFilter)),H.filter_defaultFilter&&c.iQuery.test(J.defaultAnyFilter)&&(I.anyMatchFilter=b.defaultFilter(I.anyMatchFilter,J.defaultAnyFilter),z=!1),I.iAnyMatchFilter=H.filter_ignoreCase&&G.ignoreCase?I.anyMatchFilter.toLowerCase():I.anyMatchFilter),m=0;m<h;m++)if(E=k[m].className,q=m&&c.child.test(E),!(q||z&&c.filtered.test(E))){if(I.$row=k.eq(m),I.rowIndex=m,I.cacheArray=i[m],j=I.cacheArray[G.columns],I.rawArray=j.raw,I.childRowText="",!H.filter_childByColumn){for(E="",r=j.child,x=0;x<r.length;x++)E+=" "+r[x].join(" ")||"";I.childRowText=H.filter_childRows?H.filter_ignoreCase?E.toLowerCase():E:""}if(t=!1,u=b.processRow(G,I,J),l=j.$row,w=!!u,r=j.$row.filter(":gt(0)"),H.filter_childRows&&r.length){if(H.filter_childByColumn)for(H.filter_childWithSibs||(r.addClass(H.filter_filteredRow),l=l.eq(0)),x=0;x<r.length;x++)I.$row=r.eq(x),I.cacheArray=j.child[x],I.rawArray=I.cacheArray,w=b.processRow(G,I,J),t=t||w,!H.filter_childWithSibs&&w&&r.eq(x).removeClass(H.filter_filteredRow);t=t||u}else t=w;l.toggleClass(H.filter_filteredRow,!t)[0].display=t?"":"none"}}G.filteredRows+=k.not("."+H.filter_filteredRow).length,G.totalRows+=k.length,d.processTbody(e,o,!1)}G.lastCombinedFilter=g,G.lastSearch=F,G.$table.data("lastSearch",F),H.filter_saveFilters&&d.storage&&d.storage(e,"tablesorter-filters",b.processFilters(F,!0)),G.debug&&console.log("Completed filter widget search"+d.benchmark(v)),H.filter_initialized&&(G.$table.triggerHandler("filterBeforeEnd",G),G.$table.triggerHandler("filterEnd",G)),setTimeout(function(){d.applyWidget(G.table)},0)}},getOptionSource:function(c,e,f){c=a(c)[0];var g=c.config,h=g.widgetOptions,i=!1,j=h.filter_selectSource,k=g.$table.data("lastSearch")||[],l="function"==typeof j||d.getColumnData(c,j,e);
if(f&&""!==k[e]&&(f=!1),l===!0)i=j(c,e,f);else{if(l instanceof a||"string"===a.type(l)&&l.indexOf("</option>")>=0)return l;a.isArray(l)?i=l:"object"===a.type(j)&&l&&(i=l(c,e,f))}return i===!1&&(i=b.getOptions(c,e,f)),b.processOptions(c,e,i)},processOptions:function(b,c,e){if(!a.isArray(e))return!1;b=a(b)[0];var f,g,h,i,j,k,l=b.config,m="undefined"!=typeof c&&null!==c&&c>=0&&c<l.columns,n=!!m&&l.$headerIndexed[c].hasClass("filter-select-sort-desc"),o=[];if(e=a.grep(e,function(b,c){return!!b.text||a.inArray(b,e)===c}),m&&l.$headerIndexed[c].hasClass("filter-select-nosort"))return e;for(i=e.length,h=0;h<i;h++)g=e[h],k=g.text?g.text:g,j=(m&&l.parsers&&l.parsers.length&&l.parsers[c].format(k,b,[],c)||k).toString(),j=l.widgetOptions.filter_ignoreCase?j.toLowerCase():j,g.text?(g.parsed=j,o[o.length]=g):o[o.length]={text:g,parsed:j};for(f=l.textSorter||"",o.sort(function(a,e){var g=n?e.parsed:a.parsed,h=n?a.parsed:e.parsed;return m&&"function"==typeof f?f(g,h,!0,c,b):m&&"object"==typeof f&&f.hasOwnProperty(c)?f[c](g,h,!0,c,b):!d.sortNatural||d.sortNatural(g,h)}),e=[],i=o.length,h=0;h<i;h++)e[e.length]=o[h];return e},getOptions:function(b,c,e){b=a(b)[0];var f,g,h,i,j,k,l,m,n=b.config,o=n.widgetOptions,p=[];for(g=0;g<n.$tbodies.length;g++)for(j=n.cache[g],h=n.cache[g].normalized.length,f=0;f<h;f++)if(i=j.row?j.row[f]:j.normalized[f][n.columns].$row[0],!e||!i.className.match(o.filter_filteredRow))if(o.filter_useParsedData||n.parsers[c].parsed||n.$headerIndexed[c].hasClass("filter-parsed")){if(p[p.length]=""+j.normalized[f][c],o.filter_childRows&&o.filter_childByColumn)for(m=j.normalized[f][n.columns].$row.length-1,k=0;k<m;k++)p[p.length]=""+j.normalized[f][n.columns].child[k][c]}else if(p[p.length]=j.normalized[f][n.columns].raw[c],o.filter_childRows&&o.filter_childByColumn)for(m=j.normalized[f][n.columns].$row.length,k=1;k<m;k++)l=j.normalized[f][n.columns].$row.eq(k).children().eq(c),p[p.length]=""+d.getElementText(n,l,c);return p},buildSelect:function(d,f,g,h,i){if(d=a(d)[0],f=parseInt(f,10),d.config.cache&&!a.isEmptyObject(d.config.cache)){var j,k,l,m,n,o,p,q=d.config,r=q.widgetOptions,s=q.$headerIndexed[f],t='<option value="">'+(s.data("placeholder")||s.attr("data-placeholder")||r.filter_placeholder.select||"")+"</option>",u=q.$table.find("thead").find("select."+e.filter+'[data-column="'+f+'"]').val();if("undefined"!=typeof g&&""!==g||(g=b.getOptionSource(d,f,i)),a.isArray(g)){for(j=0;j<g.length;j++)if(p=g[j],p.text){p["data-function-name"]="undefined"==typeof p.value?p.text:p.value,t+="<option";for(k in p)p.hasOwnProperty(k)&&"text"!==k&&(t+=" "+k+'="'+p[k]+'"');p.value||(t+=' value="'+p.text+'"'),t+=">"+p.text+"</option>"}else""+p!="[object Object]"&&(l=p=(""+p).replace(c.quote,"&quot;"),k=l,l.indexOf(r.filter_selectSourceSeparator)>=0&&(m=l.split(r.filter_selectSourceSeparator),k=m[0],l=m[1]),t+=""!==p?"<option "+(k===l?"":'data-function-name="'+p+'" ')+'value="'+k+'">'+l+"</option>":"");g=[]}n=(q.$filters?q.$filters:q.$table.children("thead")).find("."+e.filter),r.filter_$externalFilters&&(n=n&&n.length?n.add(r.filter_$externalFilters):r.filter_$externalFilters),o=n.filter('select[data-column="'+f+'"]'),o.length&&(o[h?"html":"append"](t),a.isArray(g)||o.append(g).val(u),o.val(u))}},buildDefault:function(a,c){var e,f,g,h=a.config,i=h.widgetOptions,j=h.columns;for(e=0;e<j;e++)f=h.$headerIndexed[e],g=!(f.hasClass("filter-false")||f.hasClass("parser-false")),(f.hasClass("filter-select")||d.getColumnData(a,i.filter_functions,e)===!0)&&g&&b.buildSelect(a,e,"",c,f.hasClass(i.filter_onlyAvail))}},c=b.regex,d.getFilters=function(c,d,f,g){var h,i,j,k,l=[],m=c?a(c)[0].config:"",n=m?m.widgetOptions:"";if(d!==!0&&n&&!n.filter_columnFilters||a.isArray(f)&&f.join("")===m.lastCombinedFilter)return a(c).data("lastSearch");if(m&&(m.$filters&&(i=m.$filters.find("."+e.filter)),n.filter_$externalFilters&&(i=i&&i.length?i.add(n.filter_$externalFilters):n.filter_$externalFilters),i&&i.length))for(l=f||[],h=0;h<m.columns+1;h++)k=h===m.columns?n.filter_anyColumnSelector+","+n.filter_multipleColumnSelector:'[data-column="'+h+'"]',j=i.filter(k),j.length&&(j=b.getLatestSearch(j),a.isArray(f)?(g&&j.length>1&&(j=j.slice(1)),h===m.columns&&(k=j.filter(n.filter_anyColumnSelector),j=k.length?k:j),j.val(f[h]).trigger("change"+m.namespace)):(l[h]=j.val()||"",h===m.columns?j.slice(1).filter('[data-column*="'+j.attr("data-column")+'"]').val(l[h]):j.slice(1).val(l[h])),h===m.columns&&j.length&&(n.filter_$anyMatch=j));return l},d.setFilters=function(c,e,f,g){var h=c?a(c)[0].config:"",i=d.getFilters(c,!0,e,g);return"undefined"==typeof f&&(f=!0),h&&f&&(h.lastCombinedFilter=null,h.lastSearch=[],b.searching(h.table,e,g),h.$table.triggerHandler("filterFomatterUpdate")),0!==i.length}}(a),function(a,b){"use strict";var c=a.tablesorter||{};a.extend(c.css,{sticky:"tablesorter-stickyHeader",stickyVis:"tablesorter-sticky-visible",stickyHide:"tablesorter-sticky-hidden",stickyWrap:"tablesorter-sticky-wrapper"}),c.addHeaderResizeEvent=function(b,c,d){if(b=a(b)[0],b.config){var e={timer:250},f=a.extend({},e,d),g=b.config,h=g.widgetOptions,i=function(a){var b,c,d,e,f,i,j=g.$headers.length;for(h.resize_flag=!0,c=[],b=0;b<j;b++)d=g.$headers.eq(b),e=d.data("savedSizes")||[0,0],f=d[0].offsetWidth,i=d[0].offsetHeight,f===e[0]&&i===e[1]||(d.data("savedSizes",[f,i]),c.push(d[0]));c.length&&a!==!1&&g.$table.triggerHandler("resize",[c]),h.resize_flag=!1};if(clearInterval(h.resize_timer),c)return h.resize_flag=!1,!1;i(!1),h.resize_timer=setInterval(function(){h.resize_flag||i()},f.timer)}},c.addWidget({id:"stickyHeaders",priority:54,options:{stickyHeaders:"",stickyHeaders_appendTo:null,stickyHeaders_attachTo:null,stickyHeaders_xScroll:null,stickyHeaders_yScroll:null,stickyHeaders_offset:0,stickyHeaders_filteredToTop:!0,stickyHeaders_cloneId:"-sticky",stickyHeaders_addResizeEvent:!0,stickyHeaders_includeCaption:!0,stickyHeaders_zIndex:2},format:function(d,e,f){if(!(e.$table.hasClass("hasStickyHeaders")||a.inArray("filter",e.widgets)>=0&&!e.$table.hasClass("hasFilters"))){var g,h,i,j,k=e.$table,l=a(f.stickyHeaders_attachTo),m=e.namespace+"stickyheaders ",n=a(f.stickyHeaders_yScroll||f.stickyHeaders_attachTo||b),o=a(f.stickyHeaders_xScroll||f.stickyHeaders_attachTo||b),p=k.children("thead:first"),q=p.children("tr").not(".sticky-false").children(),r=k.children("tfoot"),s=isNaN(f.stickyHeaders_offset)?a(f.stickyHeaders_offset):"",t=s.length?s.height()||0:parseInt(f.stickyHeaders_offset,10)||0,u=k.parent().closest("."+c.css.table).hasClass("hasStickyHeaders")?k.parent().closest("table.tablesorter")[0].config.widgetOptions.$sticky.parent():[],v=u.length?u.height():0,w=f.$sticky=k.clone().addClass("containsStickyHeaders "+c.css.sticky+" "+f.stickyHeaders+" "+e.namespace.slice(1)+"_extra_table").wrap('<div class="'+c.css.stickyWrap+'">'),x=w.parent().addClass(c.css.stickyHide).css({position:l.length?"absolute":"fixed",padding:parseInt(w.parent().parent().css("padding-left"),10),top:t+v,left:0,visibility:"hidden",zIndex:f.stickyHeaders_zIndex||2}),y=w.children("thead:first"),z="",A=0,B=function(a,c){var d,e,f,g,h,i=a.filter(":visible"),j=i.length;for(d=0;d<j;d++)g=c.filter(":visible").eq(d),h=i.eq(d),"border-box"===h.css("box-sizing")?e=h.outerWidth():"collapse"===g.css("border-collapse")?b.getComputedStyle?e=parseFloat(b.getComputedStyle(h[0],null).width):(f=parseFloat(h.css("border-width")),e=h.outerWidth()-parseFloat(h.css("padding-left"))-parseFloat(h.css("padding-right"))-f):e=h.width(),g.css({width:e,"min-width":e,"max-width":e})},C=function(){t=s.length?s.height()||0:parseInt(f.stickyHeaders_offset,10)||0,A=0,x.css({left:l.length?parseInt(l.css("padding-left"),10)||0:k.offset().left-parseInt(k.css("margin-left"),10)-o.scrollLeft()-A,width:k.outerWidth()}),B(k,w),B(q,j)},D=function(b){if(k.is(":visible")){v=u.length?u.offset().top-n.scrollTop()+u.height():0;var d=k.offset(),e=a.isWindow(n[0]),g=a.isWindow(o[0]),h=l.length?e?n.scrollTop():n.offset().top:n.scrollTop(),i=f.stickyHeaders_includeCaption?0:k.children("caption").height()||0,j=h+t+v-i,m=k.height()-(x.height()+(r.height()||0))-i,p=j>d.top&&j<d.top+m?"visible":"hidden",q={visibility:p};l.length&&(q.top=e?j-l.offset().top:l.scrollTop()),g&&(q.left=k.offset().left-parseInt(k.css("margin-left"),10)-o.scrollLeft()-A),u.length&&(q.top=(q.top||0)+t+v),x.removeClass(c.css.stickyVis+" "+c.css.stickyHide).addClass("visible"===p?c.css.stickyVis:c.css.stickyHide).css(q),(p!==z||b)&&(C(),z=p)}};if(l.length&&!l.css("position")&&l.css("position","relative"),w.attr("id")&&(w[0].id+=f.stickyHeaders_cloneId),w.find("thead:gt(0), tr.sticky-false").hide(),w.find("tbody, tfoot").remove(),w.find("caption").toggle(f.stickyHeaders_includeCaption),j=y.children().children(),w.css({height:0,width:0,margin:0}),j.find("."+c.css.resizer).remove(),k.addClass("hasStickyHeaders").bind("pagerComplete"+m,function(){C()}),c.bindEvents(d,y.children().children("."+c.css.header)),f.stickyHeaders_appendTo?a(f.stickyHeaders_appendTo).append(x):k.after(x),e.onRenderHeader)for(i=y.children("tr").children(),h=i.length,g=0;g<h;g++)e.onRenderHeader.apply(i.eq(g),[g,e,w]);o.add(n).unbind("scroll resize ".split(" ").join(m).replace(/\s+/g," ")).bind("scroll resize ".split(" ").join(m),function(a){D("resize"===a.type)}),e.$table.unbind("stickyHeadersUpdate"+m).bind("stickyHeadersUpdate"+m,function(){D(!0)}),f.stickyHeaders_addResizeEvent&&c.addHeaderResizeEvent(d),k.hasClass("hasFilters")&&f.filter_columnFilters&&(k.bind("filterEnd"+m,function(){var d=a(document.activeElement).closest("td"),g=d.parent().children().index(d);x.hasClass(c.css.stickyVis)&&f.stickyHeaders_filteredToTop&&(b.scrollTo(0,k.position().top),g>=0&&e.$filters&&e.$filters.eq(g).find("a, select, input").filter(":visible").focus())}),c.filter.bindSearch(k,j.find("."+c.css.filter)),f.filter_hideFilters&&c.filter.hideFilters(e,w)),f.stickyHeaders_addResizeEvent&&k.bind("resize"+e.namespace+"stickyheaders",function(){C()}),k.triggerHandler("stickyHeadersInit")}},remove:function(d,e,f){var g=e.namespace+"stickyheaders ";e.$table.removeClass("hasStickyHeaders").unbind("pagerComplete resize filterEnd stickyHeadersUpdate ".split(" ").join(g).replace(/\s+/g," ")).next("."+c.css.stickyWrap).remove(),f.$sticky&&f.$sticky.length&&f.$sticky.remove(),a(b).add(f.stickyHeaders_xScroll).add(f.stickyHeaders_yScroll).add(f.stickyHeaders_attachTo).unbind("scroll resize ".split(" ").join(g).replace(/\s+/g," ")),c.addHeaderResizeEvent(d,!0)}})}(a,window),function(a,b){"use strict";var c=a.tablesorter||{};a.extend(c.css,{resizableContainer:"tablesorter-resizable-container",resizableHandle:"tablesorter-resizable-handle",resizableNoSelect:"tablesorter-disableSelection",resizableStorage:"tablesorter-resizable"}),a(function(){var b="<style>body."+c.css.resizableNoSelect+" { -ms-user-select: none; -moz-user-select: -moz-none;-khtml-user-select: none; -webkit-user-select: none; user-select: none; }."+c.css.resizableContainer+" { position: relative; height: 1px; }."+c.css.resizableHandle+" { position: absolute; display: inline-block; width: 8px;top: 1px; cursor: ew-resize; z-index: 3; user-select: none; -moz-user-select: none; }</style>";a("head").append(b)}),c.resizable={init:function(b,d){if(!b.$table.hasClass("hasResizable")){b.$table.addClass("hasResizable");var e,f,g,h,i,j=b.$table,k=j.parent(),l=parseInt(j.css("margin-top"),10),m=d.resizable_vars={useStorage:c.storage&&d.resizable!==!1,$wrap:k,mouseXPosition:0,$target:null,$next:null,overflow:"auto"===k.css("overflow")||"scroll"===k.css("overflow")||"auto"===k.css("overflow-x")||"scroll"===k.css("overflow-x"),storedSizes:[]};for(c.resizableReset(b.table,!0),m.tableWidth=j.width(),m.fullWidth=Math.abs(k.width()-m.tableWidth)<20,m.useStorage&&m.overflow&&(c.storage(b.table,"tablesorter-table-original-css-width",m.tableWidth),i=c.storage(b.table,"tablesorter-table-resized-width")||"auto",c.resizable.setWidth(j,i,!0)),d.resizable_vars.storedSizes=h=(m.useStorage?c.storage(b.table,c.css.resizableStorage):[])||[],c.resizable.setWidths(b,d,h),c.resizable.updateStoredSizes(b,d),d.$resizable_container=a('<div class="'+c.css.resizableContainer+'">').css({top:l}).insertBefore(j),g=0;g<b.columns;g++)f=b.$headerIndexed[g],i=c.getColumnData(b.table,b.headers,g),e="false"===c.getData(f,i,"resizable"),e||a('<div class="'+c.css.resizableHandle+'">').appendTo(d.$resizable_container).attr({"data-column":g,unselectable:"on"}).data("header",f).bind("selectstart",!1);c.resizable.bindings(b,d)}},updateStoredSizes:function(a,b){var c,d,e=a.columns,f=b.resizable_vars;for(f.storedSizes=[],c=0;c<e;c++)d=a.$headerIndexed[c],f.storedSizes[c]=d.is(":visible")?d.width():0},setWidth:function(a,b,c){a.css({width:b,"min-width":c?b:"","max-width":c?b:""})},setWidths:function(b,d,e){var f,g,h=d.resizable_vars,i=a(b.namespace+"_extra_headers"),j=b.$table.children("colgroup").children("col");if(e=e||h.storedSizes||[],e.length){for(f=0;f<b.columns;f++)c.resizable.setWidth(b.$headerIndexed[f],e[f],h.overflow),i.length&&(g=i.eq(f).add(j.eq(f)),c.resizable.setWidth(g,e[f],h.overflow));g=a(b.namespace+"_extra_table"),g.length&&!c.hasWidget(b.table,"scroller")&&c.resizable.setWidth(g,b.$table.outerWidth(),h.overflow)}},setHandlePosition:function(b,d){var e,f=b.$table.height(),g=d.$resizable_container.children(),h=Math.floor(g.width()/2);c.hasWidget(b.table,"scroller")&&(f=0,b.$table.closest("."+c.css.scrollerWrap).children().each(function(){var b=a(this);f+=b.filter('[style*="height"]').length?b.height():b.children("table").height()})),e=b.$table.position().left,g.each(function(){var c=a(this),g=parseInt(c.attr("data-column"),10),i=b.columns-1,j=c.data("header");j&&(j.is(":visible")?(g<i||g===i&&d.resizable_addLastColumn)&&c.css({display:"inline-block",height:f,left:j.position().left-e+j.outerWidth()-h}):c.hide())})},toggleTextSelection:function(b,d,e){var f=b.namespace+"tsresize";d.resizable_vars.disabled=e,a("body").toggleClass(c.css.resizableNoSelect,e),e?a("body").attr("unselectable","on").bind("selectstart"+f,!1):a("body").removeAttr("unselectable").unbind("selectstart"+f)},bindings:function(d,e){var f=d.namespace+"tsresize";e.$resizable_container.children().bind("mousedown",function(b){var f,g=e.resizable_vars,h=a(d.namespace+"_extra_headers"),i=a(b.target).data("header");f=parseInt(i.attr("data-column"),10),g.$target=i=i.add(h.filter('[data-column="'+f+'"]')),g.target=f,g.$next=b.shiftKey||e.resizable_targetLast?i.parent().children().not(".resizable-false").filter(":last"):i.nextAll(":not(.resizable-false)").eq(0),f=parseInt(g.$next.attr("data-column"),10),g.$next=g.$next.add(h.filter('[data-column="'+f+'"]')),g.next=f,g.mouseXPosition=b.pageX,c.resizable.updateStoredSizes(d,e),c.resizable.toggleTextSelection(d,e,!0)}),a(document).bind("mousemove"+f,function(a){var b=e.resizable_vars;b.disabled&&0!==b.mouseXPosition&&b.$target&&(e.resizable_throttle?(clearTimeout(b.timer),b.timer=setTimeout(function(){c.resizable.mouseMove(d,e,a)},isNaN(e.resizable_throttle)?5:e.resizable_throttle)):c.resizable.mouseMove(d,e,a))}).bind("mouseup"+f,function(){e.resizable_vars.disabled&&(c.resizable.toggleTextSelection(d,e,!1),c.resizable.stopResize(d,e),c.resizable.setHandlePosition(d,e))}),a(b).bind("resize"+f+" resizeEnd"+f,function(){c.resizable.setHandlePosition(d,e)}),d.$table.bind("columnUpdate"+f+" pagerComplete"+f,function(){c.resizable.setHandlePosition(d,e)}).find("thead:first").add(a(d.namespace+"_extra_table").find("thead:first")).bind("contextmenu"+f,function(){var a=0===e.resizable_vars.storedSizes.length;return c.resizableReset(d.table),c.resizable.setHandlePosition(d,e),e.resizable_vars.storedSizes=[],a})},mouseMove:function(b,d,e){if(0!==d.resizable_vars.mouseXPosition&&d.resizable_vars.$target){var f,g=0,h=d.resizable_vars,i=h.$next,j=h.storedSizes[h.target],k=e.pageX-h.mouseXPosition;if(h.overflow){if(j+k>0){for(h.storedSizes[h.target]+=k,c.resizable.setWidth(h.$target,h.storedSizes[h.target],!0),f=0;f<b.columns;f++)g+=h.storedSizes[f];c.resizable.setWidth(b.$table.add(a(b.namespace+"_extra_table")),g)}i.length||(h.$wrap[0].scrollLeft=b.$table.width())}else h.fullWidth?(h.storedSizes[h.target]+=k,h.storedSizes[h.next]-=k,c.resizable.setWidths(b,d)):(h.storedSizes[h.target]+=k,c.resizable.setWidths(b,d));h.mouseXPosition=e.pageX,b.$table.triggerHandler("stickyHeadersUpdate")}},stopResize:function(a,b){var d=b.resizable_vars;c.resizable.updateStoredSizes(a,b),d.useStorage&&(c.storage(a.table,c.css.resizableStorage,d.storedSizes),c.storage(a.table,"tablesorter-table-resized-width",a.$table.width())),d.mouseXPosition=0,d.$target=d.$next=null,a.$table.triggerHandler("stickyHeadersUpdate")}},c.addWidget({id:"resizable",priority:40,options:{resizable:!0,resizable_addLastColumn:!1,resizable_widths:[],resizable_throttle:!1,resizable_targetLast:!1,resizable_fullWidth:null},init:function(a,b,d,e){c.resizable.init(d,e)},format:function(a,b,d){c.resizable.setHandlePosition(b,d)},remove:function(b,d,e,f){if(e.$resizable_container){var g=d.namespace+"tsresize";d.$table.add(a(d.namespace+"_extra_table")).removeClass("hasResizable").children("thead").unbind("contextmenu"+g),e.$resizable_container.remove(),c.resizable.toggleTextSelection(d,e,!1),c.resizableReset(b,f),a(document).unbind("mousemove"+g+" mouseup"+g)}}}),c.resizableReset=function(b,d){a(b).each(function(){var a,e,f=this.config,g=f&&f.widgetOptions,h=g.resizable_vars;if(b&&f&&f.$headerIndexed.length){for(h.overflow&&h.tableWidth&&(c.resizable.setWidth(f.$table,h.tableWidth,!0),h.useStorage&&c.storage(b,"tablesorter-table-resized-width","auto")),a=0;a<f.columns;a++)e=f.$headerIndexed[a],g.resizable_widths&&g.resizable_widths[a]?c.resizable.setWidth(e,g.resizable_widths[a],h.overflow):e.hasClass("resizable-false")||c.resizable.setWidth(e,"",h.overflow);f.$table.triggerHandler("stickyHeadersUpdate"),c.storage&&!d&&c.storage(this,c.css.resizableStorage,{})}})}}(a,window),function(a){"use strict";var b=a.tablesorter||{};b.addWidget({id:"saveSort",priority:20,options:{saveSort:!0},init:function(a,b,c,d){b.format(a,c,d,!0)},format:function(c,d,e,f){var g,h,i=d.$table,j=e.saveSort!==!1,k={sortList:d.sortList};d.debug&&(h=new Date),i.hasClass("hasSaveSort")?j&&c.hasInitialized&&b.storage&&(b.storage(c,"tablesorter-savesort",k),d.debug&&console.log("saveSort widget: Saving last sort: "+d.sortList+b.benchmark(h))):(i.addClass("hasSaveSort"),k="",b.storage&&(g=b.storage(c,"tablesorter-savesort"),k=g&&g.hasOwnProperty("sortList")&&a.isArray(g.sortList)?g.sortList:"",d.debug&&console.log('saveSort: Last sort loaded: "'+k+'"'+b.benchmark(h)),i.bind("saveSortReset",function(a){a.stopPropagation(),b.storage(c,"tablesorter-savesort","")})),f&&k&&k.length>0?d.sortList=k:c.hasInitialized&&k&&k.length>0&&b.sortOn(d,k))},remove:function(a,c){c.$table.removeClass("hasSaveSort"),b.storage&&b.storage(a,"tablesorter-savesort","")}})}(a),a.tablesorter});




var LOADER = `<img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOng9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZpZXdCb3g9IjAgMCAxMDAgMTAwIiBwcmVzZXJ2ZUFzcGVjdFJhdGlvPSJ4TWluWU1pbiBub25lIj48Zz48cGF0aCBpZD0icCIgZD0iTTMzIDQyYTEgMSAwIDAgMSA1NS0yMCAzNiAzNiAwIDAgMC01NSAyMCIvPjx1c2UgeDpocmVmPSIjcCIgdHJhbnNmb3JtPSJyb3RhdGUoNzIgNTAgNTApIi8+PHVzZSB4OmhyZWY9IiNwIiB0cmFuc2Zvcm09InJvdGF0ZSgxNDQgNTAgNTApIi8+PHVzZSB4OmhyZWY9IiNwIiB0cmFuc2Zvcm09InJvdGF0ZSgyMTYgNTAgNTApIi8+PHVzZSB4OmhyZWY9IiNwIiB0cmFuc2Zvcm09InJvdGF0ZSgyODggNTAgNTApIi8+PGFuaW1hdGVUcmFuc2Zvcm0gYXR0cmlidXRlTmFtZT0idHJhbnNmb3JtIiB0eXBlPSJyb3RhdGUiIHZhbHVlcz0iMzYwIDUwIDUwOzAgNTAgNTAiIGR1cj0iMS44cyIgcmVwZWF0Q291bnQ9ImluZGVmaW5pdGUiLz48L2c+PC9zdmc+" id="LOADER" alt="Laddar">`;


/* ##
var links="";

$(".ad:not(.filtered,.idhidden)").slice(0,21).each( function(){
$(this).find('td.name button, td.name a').remove();
var a = $(this).find('td.name').html();
console.log(a);
var l = "https://www.blocket.se/goteborg/";
var i = $(this).attr("data-ad-id");
	link=a.trim()+" → \n "+l+i+"\n"; links+=link+"\n\n";
} );

console.log(links);
*/

Storage.prototype.setObject = function (key, value) {
    this.setItem(key, JSON.stringify(value));
};

Storage.prototype.getObject = function (key) {
    var value = this.getItem(key);
    return value && JSON.parse(value);
};

function getParameter(paramName) {
	var searchString = window.location.search.substring(1),
		i, val, params = searchString.split("&");

	for (i=0;i<params.length;i++) {
		val = params[i].split("=");
		if (val[0] == paramName) {
			return val[1];
		}
	}
	return null;
}

// Add / Update a key-value pair in the URL query parameters
// updateUrlParameter(window.location, 'parameter name', 'new parameter value');
function updateUrlParameter(uri, key, value) {
	// remove the hash part before operating on the uri
	var i = uri.indexOf('#');
	var hash = i === -1 ? ''  : uri.substr(i);
	uri = i === -1 ? uri : uri.substr(0, i);

	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
		uri = uri.replace(re, '$1' + key + "=" + value + '$2');
	} else {
		uri = uri + separator + key + "=" + value;
	}
	return uri + hash;  // finally append the hash as well
}

VariableSetup:{
  /* VariableSetup */
  var jqxhr = [];
  var jqxhr_num = 0;
  var page_to_load;
  var annonser_borttagna, annonser_total = 0;
  var blocket_gets = parseInt(localStorage.getItem("blocket_gets")); if( isNaN(blocket_gets) ){ blocket_gets = 0; }
  //var tempDivWithLoadedElements;
  var itemListDiv = $("div#item_list");
  var tempDivWithLoadedElements = [];
}

pageVariableSetup:{

  var current_page = getParameter("o");
  if(current_page === undefined || current_page === null)
    current_page = 1;
  else
    current_page = parseInt(getParameter("o"));
  var next_page = updateUrlParameter(window.location.href, 'o', current_page+1);

  var last_page;
  var lastPagehref = $("#pagination li.last a").attr("href");
  try{
    var searchString = lastPagehref.split("?");
  }catch(err){
    console.log(`${err.message}`);
  }

  var i, val, params = searchString[1].split("&");

  for (i=0;i<params.length;i++) {
    val = params[i].split("=");
    if (val[0] == "o") {
      last_page = val[1];
    }
  } //console.log("last_page = "+last_page);


    //! Pages to load?
    var pages_to_load = -1; //! sida 1 räknas inte


  if(last_page < (current_page + pages_to_load)){
    console.log(`Varning: Så många sidor finns inte! ${last_page} < (${current_page} + ${pages_to_load})`);
  }

  if( pages_to_load == -1 || (last_page < (current_page + pages_to_load)) )
    pages_to_load = last_page;
}

altPressed = false;
jQuery( document ).ready(function( $ ){

  $(document).on("click", "button.removeid", function(){

    if( cache[ $(this).attr("data-remove-id") ].hidden ){
      $(this).parent().parent().toggle("slow").toggleClass("idhidden");

      cache[ $(this).attr("data-remove-id") ].hidden = false;
    } else {
      $(this).parent().parent().toggle("slow").toggleClass("idhidden");
      cache[ $(this).attr("data-remove-id") ].hidden = true;
    }

    localStorage.setItem('cache', JSON.stringify(cache));
  });
  $(document).on("keydown", "*", function (e) {
    if (e.which == 27) {
      $(".idhidden").toggleClass("idhidden");
    }
  });
/*
  $(document).on("keydown", "*", function (e) {
    if (e.which == 220) {
      altPressed = true;

      $.toast({
        text: `toggled § on`,
        icon:'info'
      });

      $("tr.ad td.name a").each( function(){
        $(this).attr("data-href", $(this).attr("href"));
        $(this).removeAttr("href");
      });
    }
  });

  $(document).on("keyup", "*", function (e) {
    //console.log(e.which);

    if (e.which == 220) {

      $.toast({
        text: `toggled § off`,
        icon:'info'
      });

      $("tr.ad td.name a").each( function(){
        $(this).attr("href", $(this).attr("data-href"));
        $(this).removeAttr("data-href");
      });
    }
  });

  $(document).on("mouseenter", "tr.ad>td.name a", function(e){
      if(altPressed){
        $(this).attr("data-href", $(this).attr("href"));
        $(this).removeAttr("href");
      }
  });
  $(document).on("mouseleave", "tr.ad>td.name a", function(e){
      $(this).attr("href", $(this).attr("data-href"));
      $(this).removeAttr("data-href");

  });

  $(document).on("select", "tr.ad>td.name, tr.ad>td.name a", function(e){
      //console.log($(this));

  });
*/


});

function load_pages(){
  //! Ladda alla sidor bortom Sida #1
  for( page_to_load = current_page+1; page_to_load <= pages_to_load; page_to_load++ )
  {

    if( page_to_load == current_page)
      continue;

      console.log(` ${page_to_load} / ${pages_to_load}`);

      if(localStorage.getItem("blocket_gets") === undefined)
          localStorage.setItem("blocket_gets", "0");

      // change url without reload http://stackoverflow.com/questions/824349/modify-the-url-without-reloading-the-page

      currentLocation = window.location.protocol
                      + "//"
                      + window.location.hostname
                      + window.location.pathname
                      + window.location.search
                      + "&o="
                      + page_to_load
                      + window.location.hash;

      if( !isNaN(blocket_gets) )
      {
          blocket_gets++;
          localStorage.setItem("blocket_gets", blocket_gets);
          console.log("blocket_gets = "+blocket_gets);
      }

      tempDivWithLoadedElements[page_to_load] = $('<div>Temporary Container</div>');

      console.log(`Blocket har nu laddats ${blocket_gets} gånger.` );
      $("body").prepend("<div class='loadinggif'></div>");

      $(tempDivWithLoadedElements[page_to_load]).children().appendTo( itemListDiv );


      //! Load item_list from next page
      $.ajax({
          async: false,
          type: 'GET',
          url: currentLocation,
          success: function(data) {
              console.log("Loading item list from "+currentLocation);

              that = $( data ).find("div#item_list.list_bostad").html();

              $(itemListDiv).append( that );

              document.title = page_to_load + " av " + currentLocation;
          }
      }).always(function()
      {
          if(pages_to_load == page_to_load){
            $( "div.loadinggif").hide();
            $.toast({
              text:"All pages loaded!",
              hideAfter: false,
              icon:'success'
            });
            //scrapmeta();
          }
      });



  }
}

var cache = JSON.parse(localStorage.getItem('cache'));
$("body").append(`<div id="scrapmenu">
  <ol>
    <li><button href="#" id="scrapthis">Load: All Pages Content → (current page)</button></li>
    <li><button href="#" id="scrap">Scrap: Metadata → (cache) (id,image,url)</button></li>
    <li><button href="#" id="load">Scrap: Data → (cache)</button></li>
    <li><button href="#" id="show">Show: Cache</button></li>
  </ol>


</div>`);
$("#scrap").click( function(){ scrapmeta(); });
$("#load").click( function(){ loadstuff(); });
$("#show").click( function(){  $("#show").append(LOADER);  showcache();});
$("#scrapthis").click( function(){ load_pages(); });

function scrapmeta() {
  cache = JSON.parse(localStorage.getItem('cache'));
  var arrNew = [], arrExist = [];

  $( "div[itemtype*='http://schema.org/Offer'][id*=item_]" ).each(function(i) {

    id = $(this).attr("id").split("item_")[1];

    if ( $($("html").find('body')[0]).hasClass("broker") ){ console.log("Skipped scrap of "+i+", found body.broker"); return; }
    if ( $( this ).find('.broker_list_logo span[itemprop="seller"]')[0] ){ console.log("Aborting scrap of "+i+", found .broker_list_logo"); return; }

    if (!(id in cache)){
      cache[id] = {};
      cache[id].link = $(this).find(".xiti_ad_frame").attr("href");

        date = $(this).find(".jlist_date_image").attr("datetime");
      cache[id].date = Date.parse(date)/1000;

      try{
            image = $(this).find(".media-object[onclick*='Objektbild']").attr("style");
            image = image.replace("background-image: url(", "");
            image = image.replace(");", "");
          cache[id].image = image;
      }catch(err){
        console.log(`No image for ${id}: ${err.message}`);
      }
      arrNew.push(id);
      //console.log("added " + id + " .");
    } else {
      arrExist.push(id);
      //console.log(id + " exists.");
    }
  });

  if(arrNew.length>0){
    console.log("New entries: ");
    console.log(arrNew);

    console.log(cache);
  }
  if(arrExist.length>0){
    console.log("Skipped entries: ");
    console.log(arrExist);
  }


  localStorage.setItem('cache', JSON.stringify(cache));

  $.toast({
    text:"Metadata scrapped!",
    hideAfter: false,
    icon:'success'
  });
}


String.prototype.trimEnters = function () {
    return this.replace(/^\s+|\s+$/g, '');
};

function loadstuff(idtoscrap) {
  cache = JSON.parse(localStorage.getItem('cache'));
  i=1;
  x=0;y=0;z=0;
  numtoscrap=0;
  skipscrapped=true;
  for(id in cache){
    if (cache[id].scrapped === true && skipscrapped == true){
      console.log(`${i} Skipping ID #${id} | Scrapped: ${cache[id].scrapped}`);
      x++;
      continue;
    }
    console.log(`${i} Processing ID #${id}`);

    if(id == 70902756){
      console.log(cache[id]);
    }

    if(typeof idtoscrap == "number" && id != idtoscrap){
      console.log("this is not the id we are looking for");
      continue;
    }
    if(typeof idtoscrap == "number"){
      console.log(`${id} vs scrapping specific id: ${idtoscrap}`);
      continue;
    }

    if (cache[id].scrapped !== true){

       y++;

        $.ajax({
            async: false,
            type: 'GET',
            url: cache[id].link,
            success: function(data) {
                console.log("Scrapping " + cache[id].link);

                result = $( data ).find("div.view[itemtype*='http://schema.org/Offer']").html();

                if( $(data).find('h2:contains("Hittade inte annonsen…")')[0] ){

                  $.toast({
                    title: `Note:`,
                    text: `<a href="${cache[id].link}">${id}</a> removed: it no longer exists.`,
                    hideAfter: false,
                    icon:'info'
                  });
                  console.log("deleted " + id);
                  delete cache[id];
                  return;
                } else if ( $($(data).find('body')[0]).hasClass("broker") ){
                  $.toast({
                    title: `Note:`,
                    text: `<a href="${cache[id].link}">${id}</a> removed: it is a broker ad.`,
                    hideAfter: false,
                    icon:'info'
                  });
                  delete cache[id];
                  return;
                } else {

                  try{
                    cache[id].annonsnamn = $( $(result).find('h2[itemprop="name"][class*="subject_"]')[0]).html().trim();
                  }catch (e){console.log(`${id} has no annonsnamn ${e}`); }


                  try{
                    cache[id].address = $( $(result).find('.address')[0]).html().trim();
                  }catch (e){console.log(`${id} has no ort/adress ${e}`); }

                  try{
                    cache[id].kategori = $( $(result).find('.category')[0]).html().trim();
                  }catch (e){console.log(`${id} has no category ${e}`); }

                  try{
                    cache[id].price =
                    (      $(        $(result).find('.param-price span')[0]       ).html()       .trim()      )
                    .replace(" ", "")
                    .replace(" kr /mån", "");
                  }catch (e){console.log(`${id} has no price ${e}`); }

                  try{
                  cache[id].rooms = ($( $(result).find('.price-wrapper span:contains("rum")')[0]).html()).replace(" rum", "");
                  }catch (e){console.log(`${id} has no rooms ${e}`); }

                  try{
                    cache[id].sqm = ($( $(result).find('.price-wrapper span:contains("m²")')[0]).html()).replace(" m²", "");
                  }catch (e){console.log(`${id} has no sqm ${e}`); }

                  try{
                  cache[id].text = $( $(result).find('.object-text')[0]).html().trim();
                  }catch (e){console.log(`${id} has no text ${e}`); }

                  if( typeof cache[id].date === "string" ){
                    cache[id].date = Date.parse(cache[id].date)/1000;
                  }

                  var arr = [];
                  $(result).find('.carousel-inner div.item img').each( function(i){
                      arr[i] = $( $(this)[0] ).attr("src");
                  });
                  if(arr.length > 0)
                    cache[id].images = arr;

                  cache[id].gata = $( $(result).find('ul.body-links li h3.h5')[0]).html();
                  if(cache[id].gata == "") delete cache[id].gata;

                  try{
                    cache[id].advertiser = $( $(result).find('.secondary-content-col .row .col-xs-12 h2.h4')[0]).html().replace("<span>Uthyres av:</span> ", "");
                  }catch (e){console.log(`${id} has no advertiser ${e}`); }



                  // Tags
                  if(typeof cache[id].tags == "undefined"){
                    cache[id].tags = {};
                  }

                  if( $(result).find('span.separator:contains("Uthyres möblerad")')[0] ){
                    cache[id].tags.furnished = "Uthyres möblerad";
                  }

                  if( $(result).find('span.separator:contains("Delat boende")')[0] ){
                    cache[id].tags.sharedliving = "Delat boende";
                  }

                  if( $(result).find('span.separator:contains("Husdjur tillåtet")')[0] ){
                    cache[id].tags.petsallowed = "Husdjur tillåtet";
                  }

                  if( $(result).find('span.separator:contains("Uthyres korttid")')[0] ){
                    cache[id].tags.shortstay = "Uthyres korttid";
                  }

                  if( $(result).find('span.separator:contains("Uthyres tillsvidare")')[0] ){
                    cache[id].tags.typecontract = "Uthyres tillsvidare";
                  }

                  cache[id].scrapped = true;
                  z++;

                }
            }
        }).always(function() {


        });

      } // else

      localStorage.setItem('cache', JSON.stringify(cache));
      console.log(cache[id]);

      $.toast({
        text: `${id} scrapped successfully!`,
        icon:'success'
      });

      if(i == numtoscrap && numtoscrap != 0){
        break;
      }
      i++;

    }

    $.toast({
      text: `Data scrapped into Cache`,
      hideAfter: false,
      icon:'success'
    });





  console.log(`Done with object: Skipped ${x}. Found ${y}. Parsed ${z}`);
  console.log(cache);
}




function showcache() {
  cache = JSON.parse(localStorage.getItem('cache'));
  $("body").html(`
    <div style="font-weight:bold;font-size:13pt;">Blocket Hyresbostäder Cache (${Object.keys(cache).length} ads)</div>
    <form id="filter">
      Query String:<br>
      <input type="search" class="searchfilter" data-column="all" placeholder="filter ads.."><input type="reset" class="reset" value="Reset">
      <br>(Match any column)
      <br>
      <table id="types" class="tablesorter-blue" data-sortlist="[[0,0]]">
				<thead>
					<tr>
            <th>Priority</th>
            <th>Type</th>
            <th class="sorter-false">Designator</th>
          </tr>
				</thead>
				<tbody>
					<tr><td>1</td><td>regex</td><td><code>/./mig</code></td></tr>
					<tr><td>2</td><td>operators</td><td><code>&lt; &lt;= &gt;= &gt;</code></td></tr>
					<tr><td>3</td><td>not</td><td><code>!</code> or <code>!=</code></td></tr>
					<tr><td>4</td><td>exact</td><td><code>"</code> or <code>=</code></td></tr>
					<tr><td>5</td><td>and</td><td><code>&nbsp;&amp;&amp;&nbsp;</code> or <code>&nbsp;and&nbsp;</code></td></tr>
					<tr><td>6</td><td>range</td><td><code>&nbsp;-&nbsp;</code> or <code>&nbsp;to&nbsp;</code></td></tr>
					<tr><td>7</td><td>wild</td><td><code>*</code> or <code>?</code></td></tr>
					<tr><td>7</td><td>or</td><td><code>|</code> or <code>&nbsp;or&nbsp;</code></td></tr>
					<tr><td>8</td><td>fuzzy</td><td><code>~</code></td></tr>
				</tbody>
			</table>

    </form>
    <table class="tablesorter" id="list">
      <thead>
        <tr>
          <th>Annonsnamn</th>
          <th>Datum</th>
          <th>Kontakt</th>
          <th>Ort</th>
          <th>Kategori</th>
          <th>Pris</th>
          <th>Rum</th>
          <th>Storlek (sqm)</th>
          <th class="sorter-false">Text</th>
          <!--<th>Images</th>-->
          <th>Tags</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>`);
  for(id in cache){
    //if(typeof cache[id].text != "string") loadstuff(id);
    //if(typeof cache[id].price != "string") loadstuff(id);

    if(typeof cache[id].annonsnamn === "undefined" || typeof cache[id].advertiser === "undefined"){
      delete cache[id];
      console.log(cache[id]);
      localStorage.setItem('cache', JSON.stringify(cache));
      continue;

    }

    images = cache[id].images;
    imagesf = `<div class="images clearfix">`;
    if(typeof images != "undefined" ){
      for(var i = 0; i < images.length; i++){
        var currImg = cache[id].images[i];
        imagesf += `<img class="derp" src="${currImg}">`;
      }
    }
    imagesf += "</div>";

    image = "";
    if(typeof cache[id].image != "undefined")
      image = `<img src="${cache[id].image}">`;

    localStorage.setItem('cache', JSON.stringify(cache));

    tags = "<ul>";
    for(var tag in cache[id].tags){
        tagc = cache[id].tags[tag];
        tags += `<li><span class="${tag}">${tagc}</span></li>`;
    }
    tags += "</ul>";

    var sqm = "";
    if(typeof cache[id].sqm != "undefined")
      var sqm = `${cache[id].sqm}`;

    var rum = "";
    if(typeof cache[id].rooms != "undefined")
      var rum = `${cache[id].rooms}`;

    var price = "";
    if(typeof cache[id].price != "undefined")
      var price = `${cache[id].price}`;

    var gata = "";
    if(typeof cache[id].gata != "undefined")
      var gata = `<div class="gata" style="background-color: rgba(166, 232, 73, 0.23);">Gata: ${cache[id].gata}</div>`;

      // Create a new JavaScript Date object based on the timestamp
      // multiplied by 1000 so that the argument is in milliseconds, not seconds.
      var formattedTime = "";
      if(typeof cache[id].date != "undefined"){
        var date = new Date(cache[id].date*1000);
        var monthsArr = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        var hours = (date.getHours()<10?'<span class="trailingzero">0</span>':'') + date.getHours();
        var minutes = (date.getMinutes()<10?'<span class="trailingzero">0</span>':'') + date.getMinutes();
        var seconds = (date.getSeconds()<10?'<span class="trailingzero">0</span>':'') + date.getSeconds();
        var curr_day = (date.getDate()<10?'<span class="trailingzero">0</span>':'') + date.getDate();
        var curr_month = monthsArr[date.getMonth()];
        var curr_year = date.getFullYear();
        formattedTime = `${curr_month} ${curr_day}, ${curr_year} ${hours}:${minutes}:${seconds}`;


      }


      var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
      var firstDate = date;
      var secondDate = new Date();
      var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));

      if(diffDays > 60){
        console.log(`${id} removed.`);
        delete cache[id];
        continue;
      }

    var row = `
      <tr class="ad" data-ad-id="${id}">
        <td class="name">
        <button class="removeid" data-remove-id="${id}">✖</button>
          <a target="_blank" href="${cache[id].link}">
            <button class="gotoid">🔗</button>
          </a>
          ${cache[id].annonsnamn}
        </td>
        <td class="datum"><time>${formattedTime}</time></td>
        <td class="contact">
          <button title="Kontakta via Meddelande" class="sendmsg">💬</button>
          <strong>
            <a target="_blank" href="${cache[id].link}#secondary-content">
              ${cache[id].advertiser}
            </a>
          </strong>
        </td>
        <td class="ort">${cache[id].address}</td>
        <td class="kategori">${cache[id].kategori}</td>
        <td class="price">${price}</td>
        <td class="rum">${rum}</td>
        <td class="sqm">${sqm}</td>
        <td class="text">
          <button class="exp" data-id="${id}">Expand</button>
          <div class="textc">
            <!-- ${image} -->
            <h5>${cache[id].annonsnamn}</h5>
            <p>${cache[id].text}</p>
            ${gata}
            ${imagesf}
          </div>
        </td>
        <!--<td class="images">${imagesf}</td>-->
        <td class="tags">${tags}</td>
    </tr>`;

    $("#list tbody").append(row);

    if( cache[ id ].hidden){
      $(`.ad[data-ad-id="${id}"]`).addClass("idhidden");
    }


  }//for(id in cache)
  localStorage.setItem('cache', JSON.stringify(cache));

  $("#filter").append( '<textarea id="code" style="width: 200px;display: block;height: 100px;overflow: scroll;border: 1px solid rebeccapurple;}"></textarea>' );
  $("#filter #code").val( $("#list")[0].outerHTML );

  $("table#types").tablesorter({
    theme:"blue",
    widgets : [ ],
    widgetOptions : {
    }
  });

    $("table#list").tablesorter({
      theme:"default",
      widgets : [ 'filter','stickyHeaders','saveSort','columns', 'zebra' ],
      widgetOptions : {
        sortList: [1,0],
        filter_columnFilters: true,
        filter_placeholder: { search : 'Search...' },
        filter_saveFilters : true,
        filter_external: 'input.searchfilter',
        filter_reset: '.reset',
        columns : [ "primary", "secondary", "tertiary" ]
      }
    });




  $(".sendmsg").click( function(e){
    if( $(this).html() != "Skicka" ){
      $(this).parent().append(`<textarea class="sendmsgcontent clearfix"></textarea>`);
      $(this).html("Skicka");
    } else {
      var ikval,nameval,emailval;
      var adurl = $(this).parent().parent().find('.name a').attr("href");

      //get unique key to send msg
      $.ajax({
          async: false,
          type: 'GET',
          url: adurl,
          success: function(data) {
              ikval = $( data ).find('form#ad-reply-form input[name="ik"]').attr("value"); //ik 7f35418841a10e2189087a7f50be3612d7869661
              nameval = $( data ).find('form#ad-reply-form input[name="name"]').val(); //"Anton";
              emailval = $( data ).find('form#ad-reply-form input[name="email"]').val(); //"anton-olsson@outlook.com";
          },
          always: function(data)
          {

          }
      });

      idval = $(this).parent().parent().attr("data-ad-id"); //70486411
      var $this = $("[data-ad-id='"+idval+"']");

      msgval = $this.find(".sendmsgcontent").val();
      if(msgval == ""){
        $this.find(".contact").append(`<p style="color: red;">Fel: Tomt meddelande.</p>`);
        return;
      }

      recipient = ($("[data-ad-id='"+idval+"']").find(".contact a").html()).trim();
      $.ajax({
          async: false,
          type: 'POST',
          url: "https://www.blocket.se/send_ar",
          data: {
            ik: ikval,
            id: idval,
            type: "",
            name: nameval,
            email: emailval,
            adreply_body: msgval,
          },
          success: function(data)
          {
            var dataObj = JSON.parse(data);

            if(dataObj.status == "ERROR"){
              $this.find(".contact").append(`<p style="color: red;">Fel: ${dataObj.adreply_body}</p>`);

              return;
            }

              $this.find(".sendmsg").fadeOut();
              $this.find(".sendmsgcontent").attr("disabled", "disabled");
              $this.find(".contact").append(`<p style="color: green;">Meddelande skickat till ${recipient}</p>`);

          },
          always: function(data)
          {

          }
      });

    } //$(this).html() != "Skicka" ){
  }); //
} //showcache()



    $(document).on("click", "td.text .exp", function(){
        var textc = $(this).parent().find(".textc")[0];
        var id_of_ad = $(this).attr("data-id");
        console.log(cache[id_of_ad]);

        if( $(this).html() == "Expand") {
          $(this).html("Collapse");
          $(textc).toggle();
        } else {
          $(this).html("Expand");
          $(textc).toggle();
        }

    });

    $(document).on("click", "span.add, span.sub", function(e){
        $(this).remove();
    });

    $(document).on("keypress", 'form#filter input[type="search"]', function (e) {
      if (e.which == 13) {
        $('form#filter').submit();
        e.preventDefault();
        return false;

      }

    });

    $(document).on("submit", 'form#filter', function( e ) {
      e.preventDefault();
      var val = $( 'input[type="search"]' ).val();

      if ( val.substr(0,1) == "-"  ) {
        $(this).append(`<span class="sub">${val.substr(1)}</span>`);
        return;
      } else if ( val.substr(0,1) == "+"  ) {
        $(this).append(`<span class="add">${val.substr(1)}</span>`);
        return;
      } else {
        $(this).append(`<span class="add">${val}</span>`);
        return;
      }
      $('input[type="search"]').val("");
      return false;
    });


cache = JSON.parse(localStorage.getItem('cache'));
console.log(cache);
console.log(`Cache contains ` + Object.keys(cache).length + ` ads.`)
