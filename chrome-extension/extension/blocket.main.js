var LOADER = `<img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOng9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZpZXdCb3g9IjAgMCAxMDAgMTAwIiBwcmVzZXJ2ZUFzcGVjdFJhdGlvPSJ4TWluWU1pbiBub25lIj48Zz48cGF0aCBpZD0icCIgZD0iTTMzIDQyYTEgMSAwIDAgMSA1NS0yMCAzNiAzNiAwIDAgMC01NSAyMCIvPjx1c2UgeDpocmVmPSIjcCIgdHJhbnNmb3JtPSJyb3RhdGUoNzIgNTAgNTApIi8+PHVzZSB4OmhyZWY9IiNwIiB0cmFuc2Zvcm09InJvdGF0ZSgxNDQgNTAgNTApIi8+PHVzZSB4OmhyZWY9IiNwIiB0cmFuc2Zvcm09InJvdGF0ZSgyMTYgNTAgNTApIi8+PHVzZSB4OmhyZWY9IiNwIiB0cmFuc2Zvcm09InJvdGF0ZSgyODggNTAgNTApIi8+PGFuaW1hdGVUcmFuc2Zvcm0gYXR0cmlidXRlTmFtZT0idHJhbnNmb3JtIiB0eXBlPSJyb3RhdGUiIHZhbHVlcz0iMzYwIDUwIDUwOzAgNTAgNTAiIGR1cj0iMS44cyIgcmVwZWF0Q291bnQ9ImluZGVmaW5pdGUiLz48L2c+PC9zdmc+" id="LOADER" alt="Laddar">`;


/* ##
var links="";

$(".ad:not(.filtered,.idhidden)").slice(0,21).each( function(){
$(this).find('td.name button, td.name a').remove();
var a = $(this).find('td.name').html();
console.log(a);
var l = "https://www.blocket.se/goteborg/";
var i = $(this).attr("data-ad-id");
	link=a.trim()+" → \n "+l+i+"\n"; links+=link+"\n\n";
} );

console.log(links);
*/

Storage.prototype.setObject = function (key, value) {
    this.setItem(key, JSON.stringify(value));
};

Storage.prototype.getObject = function (key) {
    var value = this.getItem(key);
    return value && JSON.parse(value);
};

function getParameter(paramName) {
	var searchString = window.location.search.substring(1),
		i, val, params = searchString.split("&");

	for (i=0;i<params.length;i++) {
		val = params[i].split("=");
		if (val[0] == paramName) {
			return val[1];
		}
	}
	return null;
}

// Add / Update a key-value pair in the URL query parameters
// updateUrlParameter(window.location, 'parameter name', 'new parameter value');
function updateUrlParameter(uri, key, value) {
	// remove the hash part before operating on the uri
	var i = uri.indexOf('#');
	var hash = i === -1 ? ''  : uri.substr(i);
	uri = i === -1 ? uri : uri.substr(0, i);

	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
		uri = uri.replace(re, '$1' + key + "=" + value + '$2');
	} else {
		uri = uri + separator + key + "=" + value;
	}
	return uri + hash;  // finally append the hash as well
}

VariableSetup:{
  /* VariableSetup */
  var jqxhr = [];
  var jqxhr_num = 0;
  var page_to_load;
  var annonser_borttagna, annonser_total = 0;
  var blocket_gets = parseInt(localStorage.getItem("blocket_gets")); if( isNaN(blocket_gets) ){ blocket_gets = 0; }
  //var tempDivWithLoadedElements;
  var itemListDiv = $("div#item_list");
  var tempDivWithLoadedElements = [];
}

pageVariableSetup:{

  var current_page = getParameter("o");
  if(current_page === undefined || current_page === null)
    current_page = 1;
  else
    current_page = parseInt(getParameter("o"));
  var next_page = updateUrlParameter(window.location.href, 'o', current_page+1);

  var last_page;
  var lastPagehref = $("#pagination li.last a").attr("href");
  try{
    var searchString = lastPagehref.split("?");
  }catch(err){
    console.log(`${err.message}`);
  }

  var i, val, params = searchString[1].split("&");

  for (i=0;i<params.length;i++) {
    val = params[i].split("=");
    if (val[0] == "o") {
      last_page = val[1];
    }
  } //console.log("last_page = "+last_page);


    //! Pages to load?
    var pages_to_load = -1; //! sida 1 räknas inte


  if(last_page < (current_page + pages_to_load)){
    console.log(`Varning: Så många sidor finns inte! ${last_page} < (${current_page} + ${pages_to_load})`);
  }

  if( pages_to_load == -1 || (last_page < (current_page + pages_to_load)) )
    pages_to_load = last_page;
}

altPressed = false;
jQuery( document ).ready(function( $ ){

  $(document).on("click", "button.removeid", function(){

    if( cache[ $(this).attr("data-remove-id") ].hidden ){
      $(this).parent().parent().toggle("slow").toggleClass("idhidden");

      cache[ $(this).attr("data-remove-id") ].hidden = false;
    } else {
      $(this).parent().parent().toggle("slow").toggleClass("idhidden");
      cache[ $(this).attr("data-remove-id") ].hidden = true;
    }

    localStorage.setItem('cache', JSON.stringify(cache));
  });
  $(document).on("keydown", "*", function (e) {
    if (e.which == 27) {
      $(".idhidden").toggleClass("idhidden");
    }
  });
/*
  $(document).on("keydown", "*", function (e) {
    if (e.which == 220) {
      altPressed = true;

      $.toast({
        text: `toggled § on`,
        icon:'info'
      });

      $("tr.ad td.name a").each( function(){
        $(this).attr("data-href", $(this).attr("href"));
        $(this).removeAttr("href");
      });
    }
  });

  $(document).on("keyup", "*", function (e) {
    //console.log(e.which);

    if (e.which == 220) {

      $.toast({
        text: `toggled § off`,
        icon:'info'
      });

      $("tr.ad td.name a").each( function(){
        $(this).attr("href", $(this).attr("data-href"));
        $(this).removeAttr("data-href");
      });
    }
  });

  $(document).on("mouseenter", "tr.ad>td.name a", function(e){
      if(altPressed){
        $(this).attr("data-href", $(this).attr("href"));
        $(this).removeAttr("href");
      }
  });
  $(document).on("mouseleave", "tr.ad>td.name a", function(e){
      $(this).attr("href", $(this).attr("data-href"));
      $(this).removeAttr("data-href");

  });

  $(document).on("select", "tr.ad>td.name, tr.ad>td.name a", function(e){
      //console.log($(this));

  });
*/


});

function load_pages(){
  //! Ladda alla sidor bortom Sida #1
  for( page_to_load = current_page+1; page_to_load <= pages_to_load; page_to_load++ )
  {

    if( page_to_load == current_page)
      continue;

      console.log(` ${page_to_load} / ${pages_to_load}`);

      if(localStorage.getItem("blocket_gets") === undefined)
          localStorage.setItem("blocket_gets", "0");

      // change url without reload http://stackoverflow.com/questions/824349/modify-the-url-without-reloading-the-page

      currentLocation = window.location.protocol
                      + "//"
                      + window.location.hostname
                      + window.location.pathname
                      + window.location.search
                      + "&o="
                      + page_to_load
                      + window.location.hash;

      if( !isNaN(blocket_gets) )
      {
          blocket_gets++;
          localStorage.setItem("blocket_gets", blocket_gets);
          console.log("blocket_gets = "+blocket_gets);
      }

      tempDivWithLoadedElements[page_to_load] = $('<div>Temporary Container</div>');

      console.log(`Blocket har nu laddats ${blocket_gets} gånger.` );
      $("body").prepend("<div class='loadinggif'></div>");

      $(tempDivWithLoadedElements[page_to_load]).children().appendTo( itemListDiv );


      //! Load item_list from next page
      $.ajax({
          async: false,
          type: 'GET',
          url: currentLocation,
          success: function(data) {
              console.log("Loading item list from "+currentLocation);

              that = $( data ).find("div#item_list.list_bostad").html();

              $(itemListDiv).append( that );

              document.title = page_to_load + " av " + currentLocation;
          }
      }).always(function()
      {
          if(pages_to_load == page_to_load){
            $( "div.loadinggif").hide();
            $.toast({
              text:"All pages loaded!",
              hideAfter: false,
              icon:'success'
            });
            //scrapmeta();
          }
      });



  }
}

var cache = JSON.parse(localStorage.getItem('cache'));
$("body").append(`<div id="scrapmenu">
  <ol>
    <li><button href="#" id="scrapthis">Load: All Pages Content → (current page)</button></li>
    <li><button href="#" id="scrap">Scrap: Metadata → (cache) (id,image,url)</button></li>
    <li><button href="#" id="load">Scrap: Data → (cache)</button></li>
    <li><button href="#" id="show">Show: Cache</button></li>
  </ol>


</div>`);
$("#scrap").click( function(){ scrapmeta(); });
$("#load").click( function(){ loadstuff(); });
$("#show").click( function(){  $("#show").append(LOADER);  showcache();});
$("#scrapthis").click( function(){ load_pages(); });

function scrapmeta() {
  cache = JSON.parse(localStorage.getItem('cache'));
  var arrNew = [], arrExist = [];

  $( "div[itemtype*='http://schema.org/Offer'][id*=item_]" ).each(function(i) {

    id = $(this).attr("id").split("item_")[1];

    if ( $($("html").find('body')[0]).hasClass("broker") ){ console.log("Skipped scrap of "+i+", found body.broker"); return; }
    if ( $( this ).find('.broker_list_logo span[itemprop="seller"]')[0] ){ console.log("Aborting scrap of "+i+", found .broker_list_logo"); return; }

    if (!(id in cache)){
      cache[id] = {};
      cache[id].link = $(this).find(".xiti_ad_frame").attr("href");

        date = $(this).find(".jlist_date_image").attr("datetime");
      cache[id].date = Date.parse(date)/1000;

      try{
            image = $(this).find(".media-object[onclick*='Objektbild']").attr("style");
            image = image.replace("background-image: url(", "");
            image = image.replace(");", "");
          cache[id].image = image;
      }catch(err){
        console.log(`No image for ${id}: ${err.message}`);
      }
      arrNew.push(id);
      //console.log("added " + id + " .");
    } else {
      arrExist.push(id);
      //console.log(id + " exists.");
    }
  });

  if(arrNew.length>0){
    console.log("New entries: ");
    console.log(arrNew);

    console.log(cache);
  }
  if(arrExist.length>0){
    console.log("Skipped entries: ");
    console.log(arrExist);
  }


  localStorage.setItem('cache', JSON.stringify(cache));

  $.toast({
    text:"Metadata scrapped!",
    hideAfter: false,
    icon:'success'
  });
}


String.prototype.trimEnters = function () {
    return this.replace(/^\s+|\s+$/g, '');
};

function loadstuff(idtoscrap) {
  cache = JSON.parse(localStorage.getItem('cache'));
  i=1;
  x=0;y=0;z=0;
  numtoscrap=0;
  skipscrapped=true;
  for(id in cache){
    if (cache[id].scrapped === true && skipscrapped == true){
      console.log(`${i} Skipping ID #${id} | Scrapped: ${cache[id].scrapped}`);
      x++;
      continue;
    }
    console.log(`${i} Processing ID #${id}`);

    if(id == 70902756){
      console.log(cache[id]);
    }

    if(typeof idtoscrap == "number" && id != idtoscrap){
      console.log("this is not the id we are looking for");
      continue;
    }
    if(typeof idtoscrap == "number"){
      console.log(`${id} vs scrapping specific id: ${idtoscrap}`);
      continue;
    }

    if (cache[id].scrapped !== true){

       y++;

        $.ajax({
            async: false,
            type: 'GET',
            url: cache[id].link,
            success: function(data) {
                console.log("Scrapping " + cache[id].link);

                result = $( data ).find("div.view[itemtype*='http://schema.org/Offer']").html();

                if( $(data).find('h2:contains("Hittade inte annonsen…")')[0] ){

                  $.toast({
                    title: `Note:`,
                    text: `<a href="${cache[id].link}">${id}</a> removed: it no longer exists.`,
                    hideAfter: false,
                    icon:'info'
                  });
                  console.log("deleted " + id);
                  delete cache[id];
                  return;
                } else if ( $($(data).find('body')[0]).hasClass("broker") ){
                  $.toast({
                    title: `Note:`,
                    text: `<a href="${cache[id].link}">${id}</a> removed: it is a broker ad.`,
                    hideAfter: false,
                    icon:'info'
                  });
                  delete cache[id];
                  return;
                } else {

                  try{
                    cache[id].annonsnamn = $( $(result).find('h2[itemprop="name"][class*="subject_"]')[0]).html().trim();
                  }catch (e){console.log(`${id} has no annonsnamn ${e}`); }


                  try{
                    cache[id].address = $( $(result).find('.address')[0]).html().trim();
                  }catch (e){console.log(`${id} has no ort/adress ${e}`); }

                  try{
                    cache[id].kategori = $( $(result).find('.category')[0]).html().trim();
                  }catch (e){console.log(`${id} has no category ${e}`); }

                  try{
                    cache[id].price =
                    (      $(        $(result).find('.param-price span')[0]       ).html()       .trim()      )
                    .replace(" ", "")
                    .replace(" kr /mån", "");
                  }catch (e){console.log(`${id} has no price ${e}`); }

                  try{
                  cache[id].rooms = ($( $(result).find('.price-wrapper span:contains("rum")')[0]).html()).replace(" rum", "");
                  }catch (e){console.log(`${id} has no rooms ${e}`); }

                  try{
                    cache[id].sqm = ($( $(result).find('.price-wrapper span:contains("m²")')[0]).html()).replace(" m²", "");
                  }catch (e){console.log(`${id} has no sqm ${e}`); }

                  try{
                  cache[id].text = $( $(result).find('.object-text')[0]).html().trim();
                  }catch (e){console.log(`${id} has no text ${e}`); }

                  if( typeof cache[id].date === "string" ){
                    cache[id].date = Date.parse(cache[id].date)/1000;
                  }

                  var arr = [];
                  $(result).find('.carousel-inner div.item img').each( function(i){
                      arr[i] = $( $(this)[0] ).attr("src");
                  });
                  if(arr.length > 0)
                    cache[id].images = arr;

                  cache[id].gata = $( $(result).find('ul.body-links li h3.h5')[0]).html();
                  if(cache[id].gata == "") delete cache[id].gata;

                  try{
                    cache[id].advertiser = $( $(result).find('.secondary-content-col .row .col-xs-12 h2.h4')[0]).html().replace("<span>Uthyres av:</span> ", "");
                  }catch (e){console.log(`${id} has no advertiser ${e}`); }



                  // Tags
                  if(typeof cache[id].tags == "undefined"){
                    cache[id].tags = {};
                  }

                  if( $(result).find('span.separator:contains("Uthyres möblerad")')[0] ){
                    cache[id].tags.furnished = "Uthyres möblerad";
                  }

                  if( $(result).find('span.separator:contains("Delat boende")')[0] ){
                    cache[id].tags.sharedliving = "Delat boende";
                  }

                  if( $(result).find('span.separator:contains("Husdjur tillåtet")')[0] ){
                    cache[id].tags.petsallowed = "Husdjur tillåtet";
                  }

                  if( $(result).find('span.separator:contains("Uthyres korttid")')[0] ){
                    cache[id].tags.shortstay = "Uthyres korttid";
                  }

                  if( $(result).find('span.separator:contains("Uthyres tillsvidare")')[0] ){
                    cache[id].tags.typecontract = "Uthyres tillsvidare";
                  }

                  cache[id].scrapped = true;
                  z++;

                }
            }
        }).always(function() {


        });

      } // else

      localStorage.setItem('cache', JSON.stringify(cache));
      console.log(cache[id]);

      $.toast({
        text: `${id} scrapped successfully!`,
        icon:'success'
      });

      if(i == numtoscrap && numtoscrap != 0){
        break;
      }
      i++;

    }

    $.toast({
      text: `Data scrapped into Cache`,
      hideAfter: false,
      icon:'success'
    });





  console.log(`Done with object: Skipped ${x}. Found ${y}. Parsed ${z}`);
  console.log(cache);
}




function showcache() {
  cache = JSON.parse(localStorage.getItem('cache'));
  $("body").html(`
    <div style="font-weight:bold;font-size:13pt;">Blocket Hyresbostäder Cache (${Object.keys(cache).length} ads)</div>
    <form id="filter">
      Query String:<br>
      <input type="search" class="searchfilter" data-column="all" placeholder="filter ads.."><input type="reset" class="reset" value="Reset">
      <br>(Match any column)
      <br>
      <table id="types" class="tablesorter-blue" data-sortlist="[[0,0]]">
				<thead>
					<tr>
            <th>Priority</th>
            <th>Type</th>
            <th class="sorter-false">Designator</th>
          </tr>
				</thead>
				<tbody>
					<tr><td>1</td><td>regex</td><td><code>/./mig</code></td></tr>
					<tr><td>2</td><td>operators</td><td><code>&lt; &lt;= &gt;= &gt;</code></td></tr>
					<tr><td>3</td><td>not</td><td><code>!</code> or <code>!=</code></td></tr>
					<tr><td>4</td><td>exact</td><td><code>"</code> or <code>=</code></td></tr>
					<tr><td>5</td><td>and</td><td><code>&nbsp;&amp;&amp;&nbsp;</code> or <code>&nbsp;and&nbsp;</code></td></tr>
					<tr><td>6</td><td>range</td><td><code>&nbsp;-&nbsp;</code> or <code>&nbsp;to&nbsp;</code></td></tr>
					<tr><td>7</td><td>wild</td><td><code>*</code> or <code>?</code></td></tr>
					<tr><td>7</td><td>or</td><td><code>|</code> or <code>&nbsp;or&nbsp;</code></td></tr>
					<tr><td>8</td><td>fuzzy</td><td><code>~</code></td></tr>
				</tbody>
			</table>

    </form>
    <table class="tablesorter" id="list">
      <thead>
        <tr>
          <th>Annonsnamn</th>
          <th>Datum</th>
          <th>Kontakt</th>
          <th>Ort</th>
          <th>Kategori</th>
          <th>Pris</th>
          <th>Rum</th>
          <th>Storlek (sqm)</th>
          <th class="sorter-false">Text</th>
          <!--<th>Images</th>-->
          <th>Tags</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>`);
  for(id in cache){
    //if(typeof cache[id].text != "string") loadstuff(id);
    //if(typeof cache[id].price != "string") loadstuff(id);

    if(typeof cache[id].annonsnamn === "undefined" || typeof cache[id].advertiser === "undefined"){
      delete cache[id];
      console.log(cache[id]);
      localStorage.setItem('cache', JSON.stringify(cache));
      continue;

    }

    images = cache[id].images;
    imagesf = `<div class="images clearfix">`;
    if(typeof images != "undefined" ){
      for(var i = 0; i < images.length; i++){
        var currImg = cache[id].images[i];
        imagesf += `<img class="derp" src="${currImg}">`;
      }
    }
    imagesf += "</div>";

    image = "";
    if(typeof cache[id].image != "undefined")
      image = `<img src="${cache[id].image}">`;

    localStorage.setItem('cache', JSON.stringify(cache));

    tags = "<ul>";
    for(var tag in cache[id].tags){
        tagc = cache[id].tags[tag];
        tags += `<li><span class="${tag}">${tagc}</span></li>`;
    }
    tags += "</ul>";

    var sqm = "";
    if(typeof cache[id].sqm != "undefined")
      var sqm = `${cache[id].sqm}`;

    var rum = "";
    if(typeof cache[id].rooms != "undefined")
      var rum = `${cache[id].rooms}`;

    var price = "";
    if(typeof cache[id].price != "undefined")
      var price = `${cache[id].price}`;

    var gata = "";
    if(typeof cache[id].gata != "undefined")
      var gata = `<div class="gata" style="background-color: rgba(166, 232, 73, 0.23);">Gata: ${cache[id].gata}</div>`;

      // Create a new JavaScript Date object based on the timestamp
      // multiplied by 1000 so that the argument is in milliseconds, not seconds.
      var formattedTime = "";
      if(typeof cache[id].date != "undefined"){
        var date = new Date(cache[id].date*1000);
        var monthsArr = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        var hours = (date.getHours()<10?'<span class="trailingzero">0</span>':'') + date.getHours();
        var minutes = (date.getMinutes()<10?'<span class="trailingzero">0</span>':'') + date.getMinutes();
        var seconds = (date.getSeconds()<10?'<span class="trailingzero">0</span>':'') + date.getSeconds();
        var curr_day = (date.getDate()<10?'<span class="trailingzero">0</span>':'') + date.getDate();
        var curr_month = monthsArr[date.getMonth()];
        var curr_year = date.getFullYear();
        formattedTime = `${curr_month} ${curr_day}, ${curr_year} ${hours}:${minutes}:${seconds}`;


      }


      var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
      var firstDate = date;
      var secondDate = new Date();
      var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));

      if(diffDays > 60){
        console.log(`${id} removed.`);
        delete cache[id];
        continue;
      }

    var row = `
      <tr class="ad" data-ad-id="${id}">
        <td class="name">
        <button class="removeid" data-remove-id="${id}">✖</button>
          <a target="_blank" href="${cache[id].link}">
            <button class="gotoid">🔗</button>
          </a>
          ${cache[id].annonsnamn}
        </td>
        <td class="datum"><time>${formattedTime}</time></td>
        <td class="contact">
          <button title="Kontakta via Meddelande" class="sendmsg">💬</button>
          <strong>
            <a target="_blank" href="${cache[id].link}#secondary-content">
              ${cache[id].advertiser}
            </a>
          </strong>
        </td>
        <td class="ort">${cache[id].address}</td>
        <td class="kategori">${cache[id].kategori}</td>
        <td class="price">${price}</td>
        <td class="rum">${rum}</td>
        <td class="sqm">${sqm}</td>
        <td class="text">
          <button class="exp" data-id="${id}">Expand</button>
          <div class="textc">
            <!-- ${image} -->
            <h5>${cache[id].annonsnamn}</h5>
            <p>${cache[id].text}</p>
            ${gata}
            ${imagesf}
          </div>
        </td>
        <!--<td class="images">${imagesf}</td>-->
        <td class="tags">${tags}</td>
    </tr>`;

    $("#list tbody").append(row);

    if( cache[ id ].hidden){
      $(`.ad[data-ad-id="${id}"]`).addClass("idhidden");
    }


  }//for(id in cache)
  localStorage.setItem('cache', JSON.stringify(cache));

  $("#filter").append( '<textarea id="code" style="width: 200px;display: block;height: 100px;overflow: scroll;border: 1px solid rebeccapurple;}"></textarea>' );
  $("#filter #code").val( $("#list")[0].outerHTML );

  $("table#types").tablesorter({
    theme:"blue",
    widgets : [ ],
    widgetOptions : {
    }
  });

    $("table#list").tablesorter({
      theme:"default",
      widgets : [ 'filter','stickyHeaders','saveSort','columns', 'zebra' ],
      widgetOptions : {
        sortList: [1,0],
        filter_columnFilters: true,
        filter_placeholder: { search : 'Search...' },
        filter_saveFilters : true,
        filter_external: 'input.searchfilter',
        filter_reset: '.reset',
        columns : [ "primary", "secondary", "tertiary" ]
      }
    });




  $(".sendmsg").click( function(e){
    if( $(this).html() != "Skicka" ){
      $(this).parent().append(`<textarea class="sendmsgcontent clearfix"></textarea>`);
      $(this).html("Skicka");
    } else {
      var ikval,nameval,emailval;
      var adurl = $(this).parent().parent().find('.name a').attr("href");

      //get unique key to send msg
      $.ajax({
          async: false,
          type: 'GET',
          url: adurl,
          success: function(data) {
              ikval = $( data ).find('form#ad-reply-form input[name="ik"]').attr("value"); //ik 7f35418841a10e2189087a7f50be3612d7869661
              nameval = $( data ).find('form#ad-reply-form input[name="name"]').val(); //"Anton";
              emailval = $( data ).find('form#ad-reply-form input[name="email"]').val(); //"anton-olsson@outlook.com";
          },
          always: function(data)
          {

          }
      });

      idval = $(this).parent().parent().attr("data-ad-id"); //70486411
      var $this = $("[data-ad-id='"+idval+"']");

      msgval = $this.find(".sendmsgcontent").val();
      if(msgval == ""){
        $this.find(".contact").append(`<p style="color: red;">Fel: Tomt meddelande.</p>`);
        return;
      }

      recipient = ($("[data-ad-id='"+idval+"']").find(".contact a").html()).trim();
      $.ajax({
          async: false,
          type: 'POST',
          url: "https://www.blocket.se/send_ar",
          data: {
            ik: ikval,
            id: idval,
            type: "",
            name: nameval,
            email: emailval,
            adreply_body: msgval,
          },
          success: function(data)
          {
            var dataObj = JSON.parse(data);

            if(dataObj.status == "ERROR"){
              $this.find(".contact").append(`<p style="color: red;">Fel: ${dataObj.adreply_body}</p>`);

              return;
            }

              $this.find(".sendmsg").fadeOut();
              $this.find(".sendmsgcontent").attr("disabled", "disabled");
              $this.find(".contact").append(`<p style="color: green;">Meddelande skickat till ${recipient}</p>`);

          },
          always: function(data)
          {

          }
      });

    } //$(this).html() != "Skicka" ){
  }); //
} //showcache()



    $(document).on("click", "td.text .exp", function(){
        var textc = $(this).parent().find(".textc")[0];
        var id_of_ad = $(this).attr("data-id");
        console.log(cache[id_of_ad]);

        if( $(this).html() == "Expand") {
          $(this).html("Collapse");
          $(textc).toggle();
        } else {
          $(this).html("Expand");
          $(textc).toggle();
        }

    });

    $(document).on("click", "span.add, span.sub", function(e){
        $(this).remove();
    });

    $(document).on("keypress", 'form#filter input[type="search"]', function (e) {
      if (e.which == 13) {
        $('form#filter').submit();
        e.preventDefault();
        return false;

      }

    });

    $(document).on("submit", 'form#filter', function( e ) {
      e.preventDefault();
      var val = $( 'input[type="search"]' ).val();

      if ( val.substr(0,1) == "-"  ) {
        $(this).append(`<span class="sub">${val.substr(1)}</span>`);
        return;
      } else if ( val.substr(0,1) == "+"  ) {
        $(this).append(`<span class="add">${val.substr(1)}</span>`);
        return;
      } else {
        $(this).append(`<span class="add">${val}</span>`);
        return;
      }
      $('input[type="search"]').val("");
      return false;
    });


cache = JSON.parse(localStorage.getItem('cache'));
console.log(cache);
console.log(`Cache contains ` + Object.keys(cache).length + ` ads.`)
