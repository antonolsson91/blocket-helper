
Storage.prototype.setObject = function (key, value) {
    this.setItem(key, JSON.stringify(value));
};

Storage.prototype.getObject = function (key) {
    var value = this.getItem(key);
    return value && JSON.parse(value);
};

function getParameter(paramName) {
	var searchString = window.location.search.substring(1),
		i, val, params = searchString.split("&");

	for (i=0;i<params.length;i++) {
		val = params[i].split("=");
		if (val[0] == paramName) {
			return val[1];
		}
	}
	return null;
}


// Add / Update a key-value pair in the URL query parameters
// updateUrlParameter(window.location, 'parameter name', 'new parameter value');
function updateUrlParameter(uri, key, value) {
	// remove the hash part before operating on the uri
	var i = uri.indexOf('#');
	var hash = i === -1 ? ''  : uri.substr(i);
	uri = i === -1 ? uri : uri.substr(0, i);

	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
		uri = uri.replace(re, '$1' + key + "=" + value + '$2');
	} else {
		uri = uri + separator + key + "=" + value;
	}
	return uri + hash;  // finally append the hash as well
}

jQuery( document ).ready(function( $ ){



    //! Nyckelord?
    var no = $( "div[data-type='searchtext'].searchtext p.selected");
    var filter_array = [ 'Rum', 'Del', 'Inneboende', 'Kollektiv', 'Övernattning', 'Room', 'Pendlare' ];
    console.log("// Nyckelord? " + JSON.stringify(filter_array));

    //! Add Filter Box
    b = $( "div[data-type='searchtext'].searchtext:lt(1)" ).clone().appendTo( ".searchform-filters" );
    $( b ).attr('id', 'filter');
    $( "#filter" ).css( "border", "1px solid #000 ");
    $( "#filter" ).css( "background", "rgba(255,255,255,0.8" );
    $( "#filter h6" ).text("Filter");
    $( "#filter p" ).text( JSON.stringify(filter_array) );

    //! Pages to load?
    var current_page = getParameter("o");
		if(current_page === undefined || current_page === null)
			current_page = 1;
		else
			current_page = parseInt(getParameter("o"));

    var pages_to_load = 7; //! sida 1 räknas inte

	//console.log(current_page);
    /* ================================================= */
    var jqxhr = [];
    var jqxhr_num = 0;
    var page_to_load;
    var annonser_borttagna = 0;
    var annonser_total = 0;
    var blocket_gets = parseInt(localStorage.getItem("blocket_gets")); if( isNaN(blocket_gets) ){ blocket_gets = 0; }
    //var tempDivWithLoadedElements;
    var itemListDiv = $("div#item_list");
    var tempDivWithLoadedElements = [];

	//window.history.pushState({"html":document.body,"pageTitle":page_to_load},"", next_page);

	/* ================================================= */
	var next_page = updateUrlParameter(window.location.href, 'o', current_page+1);
	lastPagehref = $("#pagination li.last a").attr("href");

	var searchString = lastPagehref.split("?");

	var last_page;
	var i, val, params = searchString[1].split("&");

	for (i=0;i<params.length;i++) {
		val = params[i].split("=");
		if (val[0] == "o") {
			last_page = val[1];
		}
	} console.log("last_page = "+last_page);


	if(last_page < (current_page + pages_to_load)){
		pages_to_load = last_page;
		console.log("Varning: Så många sidor finns inte! "+last_page+" < ("+current_page+" + "+pages_to_load+")");
	}

	/* ================================================= */


	var previous;

	$( document ).on('focus', "select.sidlista", function () {
        // Store the current value on focus and on change
        previous = this.value;
    });

	$( document ).on("change", "select.sidlista", function() {
		var sida = $(this).val();
		stateObj = previous;
		location.hash = "#sida_"+sida;
		history.pushState(stateObj, "Sida "+sida, "#sida_"+sida);
		$(this).val(previous);
	});


	$(window).bind('popstate', function (event) {
		$("h1#sida_"+previous+" select option[value="+previous+"]").attr("selected", "selected");
		$(this).val()


		//console.log(event.state);
		//console.log(event);
		//event.eventObj.val('2');
	});

	$( document ).on('mouseenter', "div.media div.media-body a.xiti_ad_frame", function (e) {

    var imgE = $(this).parent().parent().find("div.media-object.pull-left div a.media-object");
		var parentE = $(this).parent().parent().find("div.media-object.pull-left");

		//console.log($(parentE).html());
		if( $(parentE).html() != "" ){
			//console.log(' $(parentE).html() != "" ');
			imgLink = $(imgE).attr("style").split("url(")[1].split(");")[0];
			$(imgE).append('<style>a[style*="'+imgLink+'"]{border:4px solid white; border-radius: 5px; content:url('+imgLink+');}</style>');
		}
  });
	$( document ).on('mouseleave', "div.media div.media-body a.xiti_ad_frame", function (e) {
		if( $(imgE).parent().parent().html() != "" ){
			var imgE = $(this).parent().parent().find("div.media-object.pull-left div a.media-object");
			$(imgE).find("style").remove();
		}
  });


    //! Ladda alla sidor bortom Sida #1
    for( page_to_load = current_page; page_to_load <= pages_to_load; page_to_load++ ){

  		sida_h = $("<h1 class='sida_h' id='sida_"+page_to_load+"'>Sida <select class='sidlista'></select> / "+pages_to_load+"</h1>");
  		sida_h_s = $(sida_h).find("select.sidlista");

  		for( i = current_page; i <= pages_to_load; i++ ){
  			if(i == page_to_load){
  				$(sida_h_s).append("<option value='"+i+"' selected>"+i+"</option>");
  			} else {
  				$(sida_h_s).append("<option value='"+i+"'>"+i+"</option>");;
  			}
  		}

  		if( page_to_load == current_page){
  			$(itemListDiv).prepend( sida_h );
  			continue;
  		}


        console.log(page_to_load + "/"+ pages_to_load);

        if(localStorage.getItem("blocket_gets") === undefined)
            localStorage.setItem("blocket_gets", "0");

        // change url without reload http://stackoverflow.com/questions/824349/modify-the-url-without-reloading-the-page

        currentLocation = window.location.protocol + "//" + window.location.hostname + window.location.pathname + window.location.search + "&o="+page_to_load + window.location.hash;

        if( !isNaN(blocket_gets) ){
            blocket_gets++;
            localStorage.setItem("blocket_gets", blocket_gets);
            console.log("blocket_gets = "+blocket_gets);
        }

        tempDivWithLoadedElements[page_to_load] = $('<div>Temporary Container</div>');
        tempDivWithLoadedElements[page_to_load].append( sida_h );
        console.log("<div>Blocket har nu laddats " + blocket_gets + " gånger.</div>" );
        $("body").prepend("<div class='loadinggif'></div>");

        $(tempDivWithLoadedElements[page_to_load]).children().appendTo( itemListDiv );


        //! Load item_list from next page
        $.ajax({
            async: false,
            type: 'GET',
            url: currentLocation,
            success: function(data) {
                console.log("Loading item list from "+currentLocation);

                that = $( data ).find("div#item_list.list_bostad").html();

                $(itemListDiv).append( that );


				document.title = page_to_load + " av " + currentLocation;
            }
        })

        /*$.get( currentLocation, function( data ) {

				console.log("Loading item list from "+currentLocation);

				that = $( data ).find("div#item_list.list_bostad").html();
				alert(page_to_load + ";" + that);
				$(".derp-"+page_to_load+"").append( that );
				$( "div#loadinggif").remove();


		})*/.always(function() {

            if(pages_to_load == page_to_load){


			$( "div.loadinggif").hide();

                //! Filtrera varje annons
                $( filter_array ).each(function(i) {
                    filter_array[i] = filter_array[i].toLowerCase();
                    //console.log("Processing " + '"' + filter_array[i] + '"');

                    if(i === 0)
                        annonser_total = $( "h4.media-heading>a.xiti_ad_heading" ).size();

                    $( "h4.media-heading>a.xiti_ad_heading" ).each(function(i2) {
						media_body = $(this).parent().parent();

						type = $(media_body).find(".cat_geo span.category").html(); //lägenhet
						loc = $(media_body).find(".cat_geo span.address").html(); //härryda
						headlineE = $(media_body).find(".media-heading"); //annonslänk
						linkE = $(media_body).find("a.item_link");
						link = $(linkE).html().trim();
						$(linkE).html(link);

						rooms = $(media_body).find(".details .li_detail_params.rooms").html(); //rum
						costE = $(media_body).find(".details .li_detail_params.monthly_rent"); //månadskostnad

            if( typeof $(costE).html() !== 'undefined' )
						      cost = $(costE).html().split(" kr/")[0].replace(/\s/g, "");

						if(cost > 999){
							cost = cost / 1000;
							cost = cost + "k<small><sup>/mån</sup></small>";
						}
						$(costE).html(cost);

						size = $(media_body).find(".details .li_detail_params.size").html(); //kvadratmeter

						time = $(media_body).find(".jlist_date_image").html(); //datum



                        target = this.text.trim();
                        target = target.toLowerCase();
                        //console.log("Identifying target: " + target);

                        if( target.indexOf(filter_array[i]) > -1 ){

                            listObj = $(this).parent().parent().parent();
                            $(listObj).css("opacity","0.2");
							$(listObj).html("<tr> 							<td>" + link + "</td> 							<td>" + cost + "</td> 							<td>" + type + "</td> 							<td>" + loc + "</td> 							<td>" + rooms + "</td> 							<td>" + size + "</td> 							<td>" + time + "</td></tr>");
							$(listObj).remove();

                            annonser_borttagna++;
                            //console.log("Target: " + target + " confirmed and destroyed.");
                        }

                    });
                });

				annonser_procent = 100-((annonser_borttagna / annonser_total).toFixed(2)*100);
                annonser_kvar = Math.abs(annonser_total - annonser_borttagna);
                $("#item_list").prepend("<div class='resultat'><strong>Resultat:</strong> " + annonser_total + " - " + annonser_borttagna + " = <strong>"+annonser_kvar+"</strong> ("+annonser_procent+"%) annons<sup>er</sup>.</div>");
                annonser_total = 0;
                annonser_borttagna = 0;
                annonser_kvar = 0;



            }
		});



	}

		//!enkel vy länk
		$("#item_list").prepend("<div id='simpleview_active'>vanlig vy</div><div id='simpleview'>byt till simpel vy?</div>");
        //!när click, css enkel vy
        $("#simpleview").click(function() {
			if(!simpleview){

				$("#simpleview").text("byt till simpel vy?");
				$("#simpleview_active").text("vanlig vy");
        $(".col-xs-9").toggleClass("simple");
				simpleview = true;
			} else {

				$("#simpleview").text("byt till vanlig vy?");
				$("#simpleview_active").text("simpel vy");
        $(".col-xs-9").toggleClass("simple");
				simpleview = false;

			}
        });


});
